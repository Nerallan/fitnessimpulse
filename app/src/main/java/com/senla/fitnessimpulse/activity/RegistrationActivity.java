package com.senla.fitnessimpulse.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.senla.fitnessimpulse.PrefManager;
import com.senla.fitnessimpulse.R;
import com.senla.fitnessimpulse.model.User;
import com.senla.fitnessimpulse.util.AuthHttp;
import com.senla.fitnessimpulse.util.HttpUserCallback;
import com.senla.fitnessimpulse.util.PermissionHandler;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener, HttpUserCallback {
    private static final String TAG = "RegistrationActivity";

    @Nullable
    private TextInputLayout nameEditLayout;
    @Nullable
    private TextInputLayout surnameEditLayout;
    @Nullable
    private TextInputLayout emailEditLayout;
    @Nullable
    private TextInputLayout passwordEditLayout;
    @Nullable
    private TextInputLayout reEnterPasswordEditLayout;
    @Nullable
    private EditText nameEditText;
    @Nullable
    private EditText surnameEditText;
    @Nullable
    private EditText emailEditText;
    @Nullable
    private EditText passwordEditText;
    @Nullable
    private EditText reEnterPasswordEditText;
    @Nullable
    private ProgressDialog progressDialog;
    @Nullable
    private Button registerButton;
    @Nullable
    private TextView goToLoginTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        initViews();
        if (registerButton != null) {
            registerButton.setOnClickListener(this);
        }
        if (goToLoginTextView != null) {
            goToLoginTextView.setOnClickListener(this);
        }
    }

    private void initViews() {
        nameEditLayout = findViewById(R.id.til_input_name);
        surnameEditLayout = findViewById(R.id.til_input_surname);
        emailEditLayout = findViewById(R.id.til_input_email);
        passwordEditLayout = findViewById(R.id.til_registration_input_password);
        reEnterPasswordEditLayout = findViewById(R.id.til_registration_reenter_password);
        nameEditText = findViewById(R.id.input_name_edit_text);
        surnameEditText = findViewById(R.id.input_surname_edit_text);
        emailEditText = findViewById(R.id.input_email_edit_text);
        passwordEditText = findViewById(R.id.input_password_edit_text);
        reEnterPasswordEditText = findViewById(R.id.confirm_password_edit_text);
        registerButton = findViewById(R.id.registration_button);
        goToLoginTextView = findViewById(R.id.login_link_text_view);
    }

    private void startProgressDialog() {
        progressDialog = new ProgressDialog(RegistrationActivity.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Create profile...");
        progressDialog.show();
    }

    private void registration() {
        Log.d(TAG, "Registration");
        if (isValidInputs()) {
            PermissionHandler permissionHandler = new PermissionHandler();
            if(permissionHandler.isNetworkAvailable(this)) {
                if (registerButton != null) {
                    registerButton.setEnabled(false);
                }
                String name = null;
                if (nameEditText != null) {
                    name = nameEditText.getText().toString();
                }
                String surname = null;
                if (surnameEditText != null) {
                    surname = surnameEditText.getText().toString();
                }
                String email = null;
                if (emailEditText != null) {
                    email = emailEditText.getText().toString();
                }
                String password = null;
                if (passwordEditText != null) {
                    password = passwordEditText.getText().toString();
                }

                startProgressDialog();
                requestRegisterUser(name, surname, email, password);
            } else {
                Toast.makeText(this, "Please, turn on internet", Toast.LENGTH_SHORT).show();
            }
        } else {
            onRegistrationFailed();
        }
    }

    private void requestRegisterUser(String name, String surname, String email, String password){
        new AuthHttp(RegistrationActivity.this).executeRegistrationTask(name, surname, email, password);
    }

    private void goToLoginScreen(){
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    private void goToMainScreen(){
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void onRegistrationSuccess() {
        if (registerButton != null) {
            registerButton.setEnabled(true);
            goToMainScreen();
        }
    }

    private void onRegistrationFailed() {
        Toast.makeText(getBaseContext(), "Registration failed", Toast.LENGTH_LONG).show();
        if (registerButton != null) {
            registerButton.setEnabled(true);
        }
    }

    private boolean isValidInputs() {
        boolean valid = true;

        if (nameEditText != null && nameEditLayout != null){
            String name = nameEditText.getText().toString();
            if (name.isEmpty() || name.length() < 3) {
                nameEditLayout.setError(getText(R.string.name_input_error));
                valid = false;
            } else {
                nameEditLayout.setError(null);
            }
        }
        if (surnameEditText != null && surnameEditLayout != null){
            String name = surnameEditText.getText().toString();
            if (name.isEmpty() || name.length() < 3) {
                surnameEditLayout.setError(getText(R.string.surname_input_error));
                valid = false;
            } else {
                surnameEditLayout.setError(null);
            }
        }
        if (emailEditText != null && emailEditLayout != null) {
            String email = emailEditText.getText().toString();
            if(email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                emailEditLayout.setError(getText(R.string.email_input_error));
                valid = false;
            } else {
                emailEditLayout.setError(null);
            }
        }
        String password = null;
        if (passwordEditText != null && passwordEditLayout != null) {
            password = passwordEditText.getText().toString();
            if(password.isEmpty() || password.length() < 4 || password.length() > 10){
                passwordEditLayout.setError(getText(R.string.password_input_error));
                valid = false;
            } else {
                passwordEditLayout.setError(null);
            }
        }
        if (reEnterPasswordEditText!= null && reEnterPasswordEditLayout != null) {
            String eEnterPassword = reEnterPasswordEditText.getText().toString();
            if(!eEnterPassword.equals(password)){
                reEnterPasswordEditLayout.setError(getText(R.string.password_not_match_error));
                valid = false;
            } else {
                reEnterPasswordEditLayout.setError(null);
            }
        }
        return valid;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.registration_button:
                // Create new user account
                registration();
                break;
            case R.id.login_link_text_view:
                // Start login activity
                goToLoginScreen();
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable) {
        if(progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        onRegistrationFailed();
        Log.d(TAG, "onFailure: " + throwable.getMessage());
        Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(User user) {
        if(progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        if (user != null){
            PrefManager.getInstance(this).saveUser(user);
            onRegistrationSuccess();
        }
    }
}
