package com.senla.fitnessimpulse.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.senla.fitnessimpulse.PrefManager;
import com.senla.fitnessimpulse.R;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = "SplashActivity";
    private static final int ANIMATION_DELAY = 3000;
    private Animation animationRotateCenter;

    @Nullable
    private ImageView iconImageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        iconImageView = findViewById(R.id.icon_image_view);
        spinIcon();

        // targetActivity can be Login or Main screen
        Class targetActivity = isUserLoggedIn() ? MainActivity.class : LoginActivity.class;
        launchScreen(targetActivity);
        Log.d(TAG, "onCreate: ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (iconImageView != null) {
            iconImageView.startAnimation(animationRotateCenter);
        }
    }

    private void spinIcon(){
        animationRotateCenter = AnimationUtils.loadAnimation(this, R.anim.icon_rotation_around);
    }

    private void launchScreen(final Class targetActivity) {
        new Handler().postDelayed(() -> {
            Intent intent = new Intent(getApplicationContext(), targetActivity);
            startActivity(intent);
            finish();
        }, ANIMATION_DELAY);
    }

    private boolean isUserLoggedIn(){
        return PrefManager.getInstance(this).getUser().getUserId() != null;
    }
}
