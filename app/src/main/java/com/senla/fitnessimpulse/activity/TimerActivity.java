package com.senla.fitnessimpulse.activity;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.senla.fitnessimpulse.R;
import com.senla.fitnessimpulse.database.DBManager;
import com.senla.fitnessimpulse.database.FitnessDbSchema;
import com.senla.fitnessimpulse.fragment.LaunchedTimerFragment;
import com.senla.fitnessimpulse.fragment.StartTimerFragment;
import com.senla.fitnessimpulse.fragment.TrackStatisticFragment;
import com.senla.fitnessimpulse.model.Location;
import com.senla.fitnessimpulse.model.Track;

import java.util.List;

public class TimerActivity extends AppCompatActivity implements
        LaunchedTimerFragment.DataToStatisticListener,
        StartTimerFragment.DataPassToLaunchListener,
        LaunchedTimerFragment.LastDbItemListener,
        LaunchedTimerFragment.AddToDbListener {
    private static final String START_TIMER_FRAGMENT = "com.senla.fitnessimpulse.fragment.StartTimerFragment";
    private static final String TRACK_STATISTICKS_FRAGMENT = "com.senla.fitnessimpulse.fragment.TrackStatisticFragment";
    private static final String LAUNCHED_TIMER_FRAGMENT = "com.senla.fitnessimpulse.fragment.LaunchedTimerFragment";

    private static final String ARGS_STATISTIC_DISTANCE = "com.senla.fitnessimpulse.fragment.STATISTIC_DISTANCE";
    private static final String ARGS_STATISTIC_TIME = "com.senla.fitnessimpulse.fragment.STATISTIC_TIME";
    private static final String ARGS_START_DATE = "com.senla.fitnessimpulse.fragment.START_DATE";
    private static final String ARGS_START_TIME = "com.senla.fitnessimpulse.fragment.START_TIME";
    private static final String TAG = "TimerActivity";

    @Nullable
    private FragmentManager manager;
    @Nullable
    private FragmentTransaction transaction;
    @Nullable
    private Toolbar toolbar;

    private DBManager dbManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);

        toolbar = findViewById(R.id.timer_activity_toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        if (savedInstanceState == null){
            replaceFragment(StartTimerFragment.newInstance(), START_TIMER_FRAGMENT);
        }
        dbManager = new DBManager(this);
        dbManager.open();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem menuItem) {
        Log.d(TAG, "onNavigationItemSelected: ");
        Fragment launchedFragment = getSupportFragmentManager().findFragmentByTag(LAUNCHED_TIMER_FRAGMENT);
        Fragment statisticFragment = getSupportFragmentManager().findFragmentByTag(TRACK_STATISTICKS_FRAGMENT);
        if (menuItem.getItemId() == android.R.id.home && launchedFragment != null && launchedFragment.isVisible()){
            Toast.makeText(this, "Press FINISH button first ", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Fragment Launched BACK PRESSED");
            return false;
        } else if (menuItem.getItemId() == android.R.id.home && statisticFragment != null && statisticFragment.isVisible()){
            getSupportFragmentManager().popBackStackImmediate();
            return true;
        } else {
            // close this activity and return to preview activity (if there is any)
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
            return super.onOptionsItemSelected(menuItem);
        }
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed: ");
        Fragment launchedFragment = getSupportFragmentManager().findFragmentByTag(LAUNCHED_TIMER_FRAGMENT);
        Fragment statisticFragment = getSupportFragmentManager().findFragmentByTag(TRACK_STATISTICKS_FRAGMENT);
        if (launchedFragment != null && launchedFragment.isVisible()) {
            Toast.makeText(this, "Press FINISH button first ", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Fragment Launched BACK PRESSED");
            return;
        } else if (statisticFragment != null && statisticFragment.isVisible()){
            getSupportFragmentManager().popBackStack();
        } else {
            // close this activity and return to preview activity (if there is any)
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
            super.onBackPressed();
        }
    }

    private void replaceFragment(Fragment fragment, String fragmentTag){
        manager = getSupportFragmentManager();
        if(manager != null) {
            transaction = manager.beginTransaction();
        }
        if (transaction != null) {
            transaction.replace(R.id.container, fragment, fragmentTag);
            transaction.commit();
        }
    }

    @Override
    public void passDataToStatisticFragment(String runTime, String runDistance) {
        TrackStatisticFragment trackStatisticFragment = TrackStatisticFragment.newInstance();
        Bundle args = new Bundle();
        args.putString(ARGS_STATISTIC_TIME, runTime);
        args.putString(ARGS_STATISTIC_DISTANCE, runDistance);
        trackStatisticFragment.setArguments(args);
        getSupportFragmentManager().popBackStack();
        getSupportFragmentManager()
            .beginTransaction()
                .setCustomAnimations(
                        R.anim.card_flip_right_enter,
                        R.anim.card_flip_right_exit,
                        R.anim.card_flip_left_enter,
                        R.anim.card_flip_left_exit)
            .replace(R.id.container, trackStatisticFragment, TRACK_STATISTICKS_FRAGMENT)
            .addToBackStack(null)
            .commit();
    }

    @Override
    public void passDataToLaunchFragment(String startDate, String startTime) {
        LaunchedTimerFragment launchedTimerFragment = LaunchedTimerFragment.newInstance();
        FragmentTransaction fragmentTransaction;
        Bundle args = new Bundle();
        args.putString(ARGS_START_DATE, startDate);
        args.putString(ARGS_START_TIME, startTime);
        launchedTimerFragment.setArguments(args);
        getSupportFragmentManager().beginTransaction()
            .replace(R.id.container, launchedTimerFragment, LAUNCHED_TIMER_FRAGMENT)
            .addToBackStack(null)
            .commit();
    }



    @Override
    public Track getLastTrackFromDb() {
        Cursor cursorLastTrack = dbManager.getLastTrack();
        cursorLastTrack.moveToFirst();
        long idInDb  = Long.parseLong(cursorLastTrack.getString(cursorLastTrack.getColumnIndex(FitnessDbSchema.TrackTable.Cols.ID)));
        String startDate  = cursorLastTrack.getString(cursorLastTrack.getColumnIndex(FitnessDbSchema.TrackTable.Cols.START_DATE));
        String startTime  = cursorLastTrack.getString(cursorLastTrack.getColumnIndex(FitnessDbSchema.TrackTable.Cols.START_TIME));
        String runDistance  = cursorLastTrack.getString(cursorLastTrack.getColumnIndex(FitnessDbSchema.TrackTable.Cols.RUN_DISTANCE));
        String runTime  = cursorLastTrack.getString(cursorLastTrack.getColumnIndex(FitnessDbSchema.TrackTable.Cols.RUN_TIME));
        String locationsString  = cursorLastTrack.getString(cursorLastTrack.getColumnIndex(FitnessDbSchema.TrackTable.Cols.LIST_LOCATIONS));
        List<Location> locaLicationList = dbManager.convertStringToListLocations(locationsString);
        long syncTimestamp = Long.parseLong(cursorLastTrack.getString(cursorLastTrack.getColumnIndex(FitnessDbSchema.TrackTable.Cols.SERVER_SYNC_TIMESTAMP)));
        return new Track(idInDb, startDate, startTime, runDistance, runTime, locaLicationList, syncTimestamp);
    }

    @Override
    public void addTrackToDb(Track track) {
        boolean databaseResult = false;
        databaseResult = dbManager.addTrack(track);
        if (databaseResult){
            Log.d(TAG, "addToDatabase: operation successfully performed");
        }
    }
}

