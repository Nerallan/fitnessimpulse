package com.senla.fitnessimpulse.activity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.senla.fitnessimpulse.PrefManager;
import com.senla.fitnessimpulse.R;
import com.senla.fitnessimpulse.database.DBManager;
import com.senla.fitnessimpulse.database.FitnessDbSchema;
import com.senla.fitnessimpulse.fragment.MapTrackFragment;
import com.senla.fitnessimpulse.fragment.NotificationChangeFragment;
import com.senla.fitnessimpulse.fragment.NotificationListFragment;
import com.senla.fitnessimpulse.fragment.TrackListFragment;
import com.senla.fitnessimpulse.model.Location;
import com.senla.fitnessimpulse.model.Notification;
import com.senla.fitnessimpulse.model.Track;
import com.senla.fitnessimpulse.model.User;

import java.util.ArrayList;
import java.util.List;

import static com.senla.fitnessimpulse.PrefManager.getInstance;

public class MainActivity extends AppCompatActivity implements
		NavigationView.OnNavigationItemSelectedListener,
		TrackListFragment.DataToMapFragmentListener,
		MapTrackFragment.TrackIdListener,
		NotificationChangeFragment.NotificationDbListener,
		TrackListFragment.TrackDbListener,
		NotificationListFragment.NotificationListDBListener {

	private static final String TRACK_LIST_FRAGMENT = "com.senla.fitnessimpulse.fragment.TrackListFragment";
	private static final String NOTIFICATION_LIST_FRAGMENT = "com.senla.fitnessimpulse.fragment.NotificationListFragment";

	private static final String ARGS_MAP_TRACK_POSITION = "com.senla.fitnessimpulse.fragment.MAP_POSITION";
	private static final String TAG = "MainActivity";

	@Nullable
	private Toolbar toolbar;
	@Nullable
	private DrawerLayout drawerLayout;
	@Nullable
	private NavigationView navigationView;
	@Nullable
	private FragmentManager manager;
	@Nullable
	private FragmentTransaction transaction;
	@Nullable
	private ActionBarDrawerToggle drawerToggle;
	private DBManager dbManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		initViews();
		updateNavHeader();
		// Set a Toolbar to replace the ActionBar.
		if(toolbar != null) {
			setSupportActionBar(toolbar);
		}
		setupDrawerToggle();
		drawerToggle = setupDrawerToggle();
		if (drawerLayout != null) {
			drawerLayout.addDrawerListener(drawerToggle);
		}
		drawerToggle.syncState();
		if (navigationView != null) {
			navigationView.setNavigationItemSelectedListener(this);
			navigationView.getMenu().getItem(0).setChecked(true);
		}

		// On orientation change savedInstanceState will not be null.
		// Use this to show hamburger or up icon based on fragment back stack.
 		if(savedInstanceState == null) {
			replaceFragment(TrackListFragment.newInstance(), TRACK_LIST_FRAGMENT, false);
 		}
		dbManager = new DBManager(this);
		dbManager.open();
		setBackButton();
	}

	@Override
	protected void onPostCreate(@Nullable Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		if (drawerToggle != null && getSupportActionBar() != null) {
			drawerToggle.syncState();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.d(TAG, "onOptionsItemSelected: ");
		if (drawerToggle != null && drawerToggle.isDrawerIndicatorEnabled() && drawerToggle.onOptionsItemSelected(item)){
			return true;
		} else {
			getSupportFragmentManager().popBackStackImmediate();
			// set color on main item in nav drawer after up toolbar button press
			if (navigationView != null) {
				colorNeedNavItem();
			}
			return true;
		}
	}

	@Override
	public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
		// Create a new fragment and specify the fragment to show based on nav item clicked
		Fragment fragment = null;
		int id = menuItem.getItemId();
		if (id == R.id.nav_main_item){
			// TrackListFragment -> NotificationListFragment -> TrackListFragment
			// delete NotificationListFragment from BackStack in such case
			if (getSupportFragmentManager().getBackStackEntryCount() > 0){
				getSupportFragmentManager().popBackStack();
			}
			fragment = TrackListFragment.newInstance();
			replaceFragment(fragment, TRACK_LIST_FRAGMENT, false);
		} else if (id == R.id.nav_notifications_item) {
			// Notification menu -> NotificationChangeFragment -> Notification menu
			// delete NotificationListFragment from BackStack in such case
			if (getSupportFragmentManager().getBackStackEntryCount() > 0){
				getSupportFragmentManager().popBackStack();
			}
			fragment = NotificationListFragment.newInstance();
			replaceFragment(fragment, NOTIFICATION_LIST_FRAGMENT,false);
		} else {
			getInstance(this).logoutUser();
			Intent loginIntent = new Intent(this, LoginActivity.class);
			startActivity(loginIntent);
			finish();
		}
		setCheckedItem(id);
		if (drawerLayout != null) {
			drawerLayout.closeDrawer(GravityCompat.START);
		}
		return true;
	}

	@Override
	public void onBackPressed() {
		Log.d(TAG, "onBackPressed: ");
		if (drawerLayout != null && drawerLayout.isDrawerOpen(GravityCompat.START)) {
			drawerLayout.closeDrawer(GravityCompat.START);
		} else {
			if (navigationView != null){
				colorNeedNavItem();
			}
			super.onBackPressed();
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			super.onKeyDown(keyCode, event);
			return true;
		}
		return false;
	}

	private void updateNavHeader() {
		navigationView = findViewById(R.id.nav_view);
		View headerView = navigationView.getHeaderView(0);
		TextView navName = headerView.findViewById(R.id.name_navdrawer);
		TextView navSurname = headerView.findViewById(R.id.surname_navdrawer);
		TextView navEmail = headerView.findViewById(R.id.email_navdrawer);
		if (PrefManager.getInstance(this).getUser().getUserId() != null) {
			User user = PrefManager.getInstance(this).getUser();
			navName.setText(user.getName());
			navSurname.setText(user.getSurname());
			navEmail.setText(user.getEmail());
		}
	}

	private void initViews() {
		toolbar = findViewById(R.id.toolbar);
		navigationView = findViewById(R.id.nav_view);
		drawerLayout = findViewById(R.id.drawer_layout);
	}

	private ActionBarDrawerToggle setupDrawerToggle() {
		// Make sure you pass in a valid toolbar reference.  ActionBarDrawToggle() does not require it
		// and will not render the hamburger icon without it.
		return new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open,  R.string.navigation_drawer_close);
	}

	private void replaceFragment(Fragment fragment, String tag, boolean needInBackStack){
		manager = getSupportFragmentManager();
		if(manager != null) {
			transaction = manager.beginTransaction();
		}
		if (transaction != null) {
			transaction.replace(R.id.container, fragment, tag);
			if (needInBackStack){
				transaction.addToBackStack(null);
			}
			transaction.commit();
		}
	}

	private void colorNeedNavItem(){
		TrackListFragment trackListFragment = (TrackListFragment) getSupportFragmentManager().findFragmentByTag(TRACK_LIST_FRAGMENT);
		NotificationListFragment notificationListFragment = (NotificationListFragment) getSupportFragmentManager().findFragmentByTag(NOTIFICATION_LIST_FRAGMENT);
		if(navigationView != null){
			if (trackListFragment != null && trackListFragment.isVisible()){
				navigationView.getMenu().getItem(0).setChecked(true);
			} else if (notificationListFragment != null && notificationListFragment.isVisible()){
				navigationView.getMenu().getItem(1).setChecked(true);
			}
		}
	}

	private void setBackButton(){
		if (manager != null) {
			manager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
				@Override
				public void onBackStackChanged() {
					if (getSupportActionBar() != null) {
						if(manager.getBackStackEntryCount() > 0) {
							if (drawerToggle != null) {
								drawerToggle.setDrawerIndicatorEnabled(false);
							}
							setSupportActionBar(toolbar);
							getSupportActionBar().setDisplayHomeAsUpEnabled(true);
						} else {
							if (drawerToggle != null) {
								drawerToggle.syncState();
								drawerToggle.setDrawerIndicatorEnabled(true);
							}
						}
					}
				}
			});
		}
	}

	private void setCheckedItem(int id){
		if (navigationView != null) {
			for (int index = 0; index < navigationView.getMenu().size(); index++) {
				MenuItem item = navigationView.getMenu().getItem(index);
				item.setChecked(id == item.getItemId());
			}
		}
	}

	@Override
	public void passTrackIdToMap(long position) {
		MapTrackFragment mapTrackFragment = MapTrackFragment.newInstance();
		Bundle args = new Bundle();
		args.putLong(ARGS_MAP_TRACK_POSITION, position);
		mapTrackFragment.setArguments(args);
		getSupportFragmentManager()
				.beginTransaction()
				.replace(R.id.container, mapTrackFragment)
				.addToBackStack(null)
				.commit();
	}

	@Override
	public List<Track> getTracksFromDb() {
		Log.d(TAG, "getTrackFromDb: ");
		// get the data and append to the list
		Cursor dataCursor = dbManager.getAllTracks();
		List<Track> trackList = new ArrayList<>();
		while(dataCursor.moveToNext()){
			long idInDb  = Long.parseLong(dataCursor.getString(dataCursor.getColumnIndex(FitnessDbSchema.TrackTable.Cols.ID)));
			String startDate  = dataCursor.getString(dataCursor.getColumnIndex(FitnessDbSchema.TrackTable.Cols.START_DATE));
			String startTime  = dataCursor.getString(dataCursor.getColumnIndex(FitnessDbSchema.TrackTable.Cols.START_TIME));
			String runDistance  = dataCursor.getString(dataCursor.getColumnIndex(FitnessDbSchema.TrackTable.Cols.RUN_DISTANCE));
			String runTime  = dataCursor.getString(dataCursor.getColumnIndex(FitnessDbSchema.TrackTable.Cols.RUN_TIME));
			String locationsString  = dataCursor.getString(dataCursor.getColumnIndex(FitnessDbSchema.TrackTable.Cols.LIST_LOCATIONS));
			List<Location> locaLicationList = dbManager.convertStringToListLocations(locationsString);
			long syncTimestamp = Long.parseLong(dataCursor.getString(dataCursor.getColumnIndex(FitnessDbSchema.TrackTable.Cols.SERVER_SYNC_TIMESTAMP)));
			trackList.add(new Track(idInDb, startDate, startTime, runDistance, runTime, locaLicationList, syncTimestamp));
		}
		return trackList;
	}

	@Override
	public Track getTrackById(long positionInDb) {
		Cursor trackCursor = dbManager.getTrackById(positionInDb);
		long idInDb = Long.parseLong(trackCursor.getString(trackCursor.getColumnIndex(FitnessDbSchema.TrackTable.Cols.ID)));
		String startDate = trackCursor.getString(trackCursor.getColumnIndex(FitnessDbSchema.TrackTable.Cols.START_DATE));
		String startTime = trackCursor.getString(trackCursor.getColumnIndex(FitnessDbSchema.TrackTable.Cols.START_TIME));
		String runDistance = trackCursor.getString(trackCursor.getColumnIndex(FitnessDbSchema.TrackTable.Cols.RUN_DISTANCE));
		String runTime = trackCursor.getString(trackCursor.getColumnIndex(FitnessDbSchema.TrackTable.Cols.RUN_TIME));
		String locations = trackCursor.getString(trackCursor.getColumnIndex(FitnessDbSchema.TrackTable.Cols.LIST_LOCATIONS));

		List<Location> locationList = dbManager.convertStringToListLocations(locations);
		long syncTimestamp = Long.parseLong(trackCursor.getString(trackCursor.getColumnIndex(FitnessDbSchema.TrackTable.Cols.SERVER_SYNC_TIMESTAMP)));
		return new Track(idInDb, startDate, startTime, runDistance, runTime, locationList, syncTimestamp);
	}

	@Override
	public boolean insertServerDataToDb(List<Track> trackList) {
		return dbManager.bulkInsertTracks(trackList);
	}

	@Override
	public void updateTimestampInDb(List<Track> trackList) {
		dbManager.bulkUpdateSyncTracks(trackList);
	}

	@Override
	public Notification getNotificationById(long position) {
		Cursor notificationCursor = dbManager.getNotificationById(position);
		long id = notificationCursor.getLong(notificationCursor.getColumnIndex(FitnessDbSchema.NotificationTable.Cols.ID));
		String title = notificationCursor.getString(notificationCursor.getColumnIndex(FitnessDbSchema.NotificationTable.Cols.TITLE));
		String date = notificationCursor.getString(notificationCursor.getColumnIndex(FitnessDbSchema.NotificationTable.Cols.DATE));
		String time = notificationCursor.getString(notificationCursor.getColumnIndex(FitnessDbSchema.NotificationTable.Cols.TIME));
		return new Notification(id, title, date, time);
	}

    @Override
    public void addNotificationToDb(Notification notification) {
        boolean databaseResult = dbManager.addNotification(notification);
        if (databaseResult){
            Log.d(TAG, "addToDatabase: operation successfully performed");
        }
    }

    @Override
    public void updateNotificationInDb(Notification notification) {
        boolean databaseResult = dbManager.updateNotification(notification, () -> {
            Log.d(TAG, "updateRowInDatabase: successful callback");
        });
        if (databaseResult){
            Log.d(TAG, "addToDatabase: operation successfully performed");
        }
    }

	@Override
	public List<Notification> getNotificationsFromDb() {
		Log.d(TAG, "getNotificationFromDb: ");
		// get the data and append to the list
		Cursor dataCursor = dbManager.getAllNotifications();
		List<Notification> notificationList = new ArrayList<>();
		while(dataCursor.moveToNext()){
			long id = dataCursor.getLong(dataCursor.getColumnIndex(FitnessDbSchema.NotificationTable.Cols.ID));
			String title  = dataCursor.getString(dataCursor.getColumnIndex(FitnessDbSchema.NotificationTable.Cols.TITLE));
			String date  = dataCursor.getString(dataCursor.getColumnIndex(FitnessDbSchema.NotificationTable.Cols.DATE));
			String time  = dataCursor.getString(dataCursor.getColumnIndex(FitnessDbSchema.NotificationTable.Cols.TIME));
			notificationList.add(new Notification(id, title, date, time));
		}
		return notificationList;
	}

	@Override
	public void deleteNotificationById(long position) {
		dbManager.deleteNotification(position);
	}
}
