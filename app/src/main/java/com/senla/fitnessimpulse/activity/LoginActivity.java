package com.senla.fitnessimpulse.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.senla.fitnessimpulse.PrefManager;
import com.senla.fitnessimpulse.R;
import com.senla.fitnessimpulse.model.User;
import com.senla.fitnessimpulse.util.AuthHttp;
import com.senla.fitnessimpulse.util.HttpUserCallback;
import com.senla.fitnessimpulse.util.PermissionHandler;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, HttpUserCallback {
    private static final String TAG = "LoginActivity";

    @Nullable
    private TextInputLayout emailEditLayout;
    @Nullable
    private TextInputLayout passwordEditLayout;
    @Nullable
    private EditText emailEditText;
    @Nullable
    private EditText passwordEditText;
    @Nullable
    private ProgressDialog progressDialog;
    @Nullable
    private Button loginButton;
    @Nullable
    private CheckBox passwordCheckBox;
    @Nullable
    private TextView goToRegisterTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViews();
        if (loginButton != null) {
            loginButton.setOnClickListener(this);
        }
        if (goToRegisterTextView != null) {
            goToRegisterTextView.setOnClickListener(this);
        }
        changePasswordVisibility();
    }

    private void initViews() {
        emailEditLayout = findViewById(R.id.til_input_email);
        emailEditText = findViewById(R.id.input_email_edit_text);
        passwordEditLayout = findViewById(R.id.til_input_password);
        passwordEditText = findViewById(R.id.input_password_edit_text);
        loginButton = findViewById(R.id.login_button);
        passwordCheckBox = findViewById(R.id.password_checkbox);
        goToRegisterTextView = findViewById(R.id.registration_link_text_view);
    }

    private void changePasswordVisibility(){
        if (passwordCheckBox != null && passwordEditText != null){
            passwordCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    passwordEditText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                } else {
                    passwordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
                }
            });
        }
    }

    private void login() {
        Log.d(TAG, "Login");
        PermissionHandler permissionHandler = new PermissionHandler();
        if (isValidInputs()) {
            if(permissionHandler.isNetworkAvailable(this)){
                if (loginButton != null) {
                    loginButton.setEnabled(false);
                }
                String email = emailEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                startProgressDialog();
                requestLoginUser(email, password);
            } else {
                Toast.makeText(this, "Please, turn on internet", Toast.LENGTH_SHORT).show();
            }
        } else {
            invalidInput();
        }
    }

    private void startProgressDialog() {
        progressDialog = new ProgressDialog(LoginActivity.this, R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();
    }

    private void requestLoginUser(String email, String password){
        new AuthHttp(LoginActivity.this).executeLoginTask(email, password);
    }

    private void goToRegistrationScreen(){
        Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    private void goToMainScreen(){
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void onLoginSuccess(){
        if (loginButton != null) {
            loginButton.setEnabled(true);
            goToMainScreen();
        }
    }

    private void invalidInput(){
        Toast.makeText(getBaseContext(), "Invalid input pattern", Toast.LENGTH_LONG).show();
        if (loginButton != null) {
            loginButton.setEnabled(true);
        }
    }

    private void onLoginFailed(){
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        if (loginButton != null) {
            loginButton.setEnabled(true);
        }
    }

    private boolean isValidInputs() {
        boolean valid = true;
        if (emailEditText != null) {
            String email = emailEditText.getText().toString();
            if(email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                if (emailEditLayout != null) {
                    emailEditLayout.setError(getText(R.string.email_input_error));
                }
                valid = false;
            } else {
                if (emailEditLayout != null) {
                    emailEditLayout.setError(null);
                }
            }
        }
        if (passwordEditText != null) {
            String password = passwordEditText.getText().toString();
            if(password.isEmpty() || password.length() < 4 || password.length() > 10){
                if (passwordEditLayout != null) {
                    passwordEditLayout.setError(getText(R.string.password_input_error));
                }
                valid = false;
            } else {
                if (passwordEditLayout != null) {
                    passwordEditLayout.setError(null);
                }
            }
        }
        return valid;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.login_button:
                // Start logging in app
                login();
                break;
            case R.id.registration_link_text_view:
                // Start the registration activity
                goToRegistrationScreen();
                break;
        }
    }

    @Override
    public void onFailure(Throwable throwable) {
        if(progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        onLoginFailed();
        Log.d(TAG, "onFailure: " + throwable.getMessage());
        Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccess(User user) {
        if(progressDialog != null && progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        if (user != null){
            PrefManager.getInstance(this).saveUser(user);
            onLoginSuccess();
        }
    }
}
