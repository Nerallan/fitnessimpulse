package com.senla.fitnessimpulse;

import android.app.Application;
import android.content.Context;


public class App extends Application {

	private static Context context;

	@Override
	public void onCreate() {
		super.onCreate();
		App.context = getApplicationContext();
	}

	public static Context getInstance() {
		return App.context;
	}
}
