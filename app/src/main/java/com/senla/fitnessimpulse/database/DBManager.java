package com.senla.fitnessimpulse.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.senla.fitnessimpulse.PrefManager;
import com.senla.fitnessimpulse.model.Location;
import com.senla.fitnessimpulse.model.Notification;
import com.senla.fitnessimpulse.model.Track;
import com.senla.fitnessimpulse.repository.NotificationRepository;
import com.senla.fitnessimpulse.repository.TrackRepository;

import java.lang.reflect.Type;
import java.util.List;

public class DBManager implements TrackRepository, NotificationRepository {

    private static final String TAG = "DBManager";

    private Context context;
    private DatabaseHelper databaseHelper;
    private SQLiteDatabase sqLiteDatabase;
    private String userId;

    public DBManager(Context context){
        this.context = context;
    }

    public interface SuccessfulUpdateListener{
        void success();
    }

    public DBManager open() throws SQLException{
        databaseHelper = new DatabaseHelper(context);
        sqLiteDatabase = databaseHelper.getWritableDatabase();
        userId = PrefManager.getInstance(context).getUser().getUserId();
        return this;
    }

    public void close() {
        databaseHelper.close();
    }

    @Override
    public Cursor getTrackById(long positionInDb) {
        String selectTrackQuery = "SELECT * FROM " + FitnessDbSchema.TrackTable.NAME + " WHERE " + FitnessDbSchema.TrackTable.Cols.USER_ID + " = '" + userId + "'" + " AND " + FitnessDbSchema.TrackTable.Cols.ID + " = " + positionInDb;
        Cursor trackCursor = sqLiteDatabase.rawQuery(selectTrackQuery, null);
        if (trackCursor != null){
            trackCursor.moveToFirst();
        }
        return trackCursor;
    }

    @Override
    public Cursor getAllTracks() {
        String selectQuery = "SELECT * FROM " + FitnessDbSchema.TrackTable.NAME + " WHERE " + FitnessDbSchema.TrackTable.Cols.USER_ID + " = '" + userId + "'";
        return sqLiteDatabase.rawQuery(selectQuery, null);
    }

    @Override
    public boolean addTrack(Track track) {
        long isSuccessful = 0;
        ContentValues contentValues = new ContentValues();
        contentValues.put(FitnessDbSchema.TrackTable.Cols.USER_ID, userId);
        contentValues.put(FitnessDbSchema.TrackTable.Cols.START_DATE, track.getStartDate());
        contentValues.put(FitnessDbSchema.TrackTable.Cols.START_TIME, track.getStartTime());
        contentValues.put(FitnessDbSchema.TrackTable.Cols.RUN_DISTANCE, track.getRunDistance());
        contentValues.put(FitnessDbSchema.TrackTable.Cols.RUN_TIME, track.getRunTime());
        contentValues.put(FitnessDbSchema.TrackTable.Cols.LIST_LOCATIONS, convertListLocationsToString(track.getLocationList()));
        contentValues.put(FitnessDbSchema.TrackTable.Cols.SERVER_SYNC_TIMESTAMP, String.valueOf(track.getServerSyncTime()));
        if(sqLiteDatabase != null){
            isSuccessful = sqLiteDatabase.insert(FitnessDbSchema.TrackTable.NAME, null, contentValues);
        }
        // if data inserted incorrectly it will return -1
        if (isSuccessful == -1){
            return false;
        } else {
            return true;
        }
    }

    public List<Location> convertStringToListLocations(String strLocations){
        Gson gson = new Gson();
        Type locationType = new TypeToken<List<Location>>(){}.getType();
        return gson.fromJson(strLocations, locationType);
    }

    private String convertListLocationsToString(List<Location> locationList){
        Gson gson = new Gson();
        String gsonListLocations = gson.toJson(locationList);
        Log.d(TAG, "convertListLocationsToString: ");
        return gsonListLocations;
    }

    @Override
    public Cursor getNotificationById(long position) {
        String selectNotificationQuery = "SELECT * FROM " + FitnessDbSchema.NotificationTable.NAME + " WHERE " + FitnessDbSchema.NotificationTable.Cols.USER_ID + " = '" + userId + "' AND " + FitnessDbSchema.NotificationTable.Cols.ID + " = " + position;
        Cursor notificationCursor = sqLiteDatabase.rawQuery(selectNotificationQuery, null);
        if (notificationCursor != null){
            notificationCursor.moveToFirst();
        }
        return notificationCursor;
    }

    @Override
    public Cursor getAllNotifications() {
        String selectQuery = "SELECT * FROM " + FitnessDbSchema.NotificationTable.NAME + " WHERE " + FitnessDbSchema.NotificationTable.Cols.USER_ID + " = '" + userId + "'";
        return sqLiteDatabase.rawQuery(selectQuery, null);
    }

    @Override
    public boolean addNotification(Notification notification) {
        long isSuccessful = 0;
        ContentValues contentValues = new ContentValues();
        contentValues.put(FitnessDbSchema.NotificationTable.Cols.USER_ID, userId);
        contentValues.put(FitnessDbSchema.NotificationTable.Cols.TITLE, notification.getTitle());
        contentValues.put(FitnessDbSchema.NotificationTable.Cols.DATE, notification.getDate());
        contentValues.put(FitnessDbSchema.NotificationTable.Cols.TIME, notification.getTime());
        if(sqLiteDatabase != null){
            isSuccessful = sqLiteDatabase.insert(FitnessDbSchema.NotificationTable.NAME, null, contentValues);
        }
        // if data inserted incorrectly it will return -1
        if (isSuccessful == -1){
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean updateNotification(Notification notification, SuccessfulUpdateListener listener){
        long isSuccessful = 0;
        ContentValues contentValues = new ContentValues();
        contentValues.put(FitnessDbSchema.NotificationTable.Cols.TITLE, notification.getTitle());
        contentValues.put(FitnessDbSchema.NotificationTable.Cols.DATE, notification.getDate());
        contentValues.put(FitnessDbSchema.NotificationTable.Cols.TIME, notification.getTime());
        String[] whereArgs = {String.valueOf(notification.getId()), String.valueOf(userId)};
        if(sqLiteDatabase != null){
            isSuccessful = sqLiteDatabase.update(FitnessDbSchema.NotificationTable.NAME, contentValues, FitnessDbSchema.NotificationTable.Cols.ID + " =? AND " + FitnessDbSchema.NotificationTable.Cols.USER_ID + " =? ", whereArgs);
            listener.success();
        }
        // if data inserted incorrectly it will return -1
        if (isSuccessful == -1){
            return false;
        } else {
            return true;
        }
    }

    public boolean deleteNotification(long position){
        long isSuccessful = 0;
        // if data inserted incorrectly it will return -1
        if (sqLiteDatabase != null){
            isSuccessful = sqLiteDatabase.delete(FitnessDbSchema.NotificationTable.NAME, FitnessDbSchema.NotificationTable.Cols.ID + "=?", new String[]{String.valueOf(position)});
        }
        if (isSuccessful == 0){
            return false;
        } else {
            return true;
        }
    }

    public boolean bulkInsertTracks(List<Track> trackList){
        long isSuccessful = 0;
        if(sqLiteDatabase != null){
            sqLiteDatabase.beginTransaction();
            try {
                ContentValues contentValues = new ContentValues();
                for (Track track : trackList){
                    contentValues.put(FitnessDbSchema.TrackTable.Cols.USER_ID, userId);
                    contentValues.put(FitnessDbSchema.TrackTable.Cols.START_DATE, track.getStartDate());
                    contentValues.put(FitnessDbSchema.TrackTable.Cols.START_TIME, track.getStartTime());
                    contentValues.put(FitnessDbSchema.TrackTable.Cols.RUN_DISTANCE, track.getRunDistance());
                    contentValues.put(FitnessDbSchema.TrackTable.Cols.RUN_TIME, track.getRunTime());
                    contentValues.put(FitnessDbSchema.TrackTable.Cols.LIST_LOCATIONS, convertListLocationsToString(track.getLocationList()));
                    contentValues.put(FitnessDbSchema.TrackTable.Cols.SERVER_SYNC_TIMESTAMP, String.valueOf(track.getServerSyncTime()));
                    isSuccessful = sqLiteDatabase.insert(FitnessDbSchema.TrackTable.NAME, null, contentValues);
                }
                sqLiteDatabase.setTransactionSuccessful();
            } finally {
                sqLiteDatabase.endTransaction();
            }
        }
        // if data inserted incorrectly it will return -1
        if (isSuccessful == -1){
            return false;
        } else {
            return true;
        }
    }

    public boolean bulkUpdateSyncTracks(List<Track> trackList){
        long isSuccessful = 0;
        if(sqLiteDatabase != null){
            sqLiteDatabase.beginTransaction();
            try {
                ContentValues contentValues = new ContentValues();
                for (Track track : trackList){
                    contentValues.put(FitnessDbSchema.TrackTable.Cols.USER_ID, userId);
                    contentValues.put(FitnessDbSchema.TrackTable.Cols.START_DATE, track.getStartDate());
                    contentValues.put(FitnessDbSchema.TrackTable.Cols.START_TIME, track.getStartTime());
                    contentValues.put(FitnessDbSchema.TrackTable.Cols.RUN_DISTANCE, track.getRunDistance());
                    contentValues.put(FitnessDbSchema.TrackTable.Cols.RUN_TIME, track.getRunTime());
                    contentValues.put(FitnessDbSchema.TrackTable.Cols.LIST_LOCATIONS, convertListLocationsToString(track.getLocationList()));
                    contentValues.put(FitnessDbSchema.TrackTable.Cols.SERVER_SYNC_TIMESTAMP, String.valueOf(track.getServerSyncTime()));
                    String[] whereArgs = {String.valueOf(track.getId()), String.valueOf(userId)};
                    isSuccessful = sqLiteDatabase.update(FitnessDbSchema.TrackTable.NAME, contentValues, FitnessDbSchema.TrackTable.Cols.ID + " =? AND " + FitnessDbSchema.NotificationTable.Cols.USER_ID + " =? ", whereArgs);
                }
                sqLiteDatabase.setTransactionSuccessful();
            } finally {
                sqLiteDatabase.endTransaction();
            }
        }
        // if data inserted incorrectly it will return -1
        if (isSuccessful == -1){
            return false;
        } else {
            return true;
        }
    }

    public Cursor getLastTrack() {
//        String selectQuery = "SELECT * FROM " + FitnessDbSchema.NotificationTable.NAME + " WHERE " + FitnessDbSchema.NotificationTable.Cols.USER_ID + " = " + " (SELECT MAX(ID) FROM " + FitnessDbSchema.TrackTable.Cols.ID + ")";
        String selectQuery = "SELECT * FROM " + FitnessDbSchema.TrackTable.NAME + " WHERE " + FitnessDbSchema.NotificationTable.Cols.USER_ID + " = '" + userId + "'" + " ORDER BY " + FitnessDbSchema.NotificationTable.Cols.ID + " DESC LIMIT 1";
        return sqLiteDatabase.rawQuery(selectQuery, null);
    }
}
