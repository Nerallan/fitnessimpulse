package com.senla.fitnessimpulse.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static com.senla.fitnessimpulse.database.FitnessDbSchema.NotificationTable;
import static com.senla.fitnessimpulse.database.FitnessDbSchema.TrackTable;
import static com.senla.fitnessimpulse.database.FitnessDbSchema.TrackTable.DATABASE_VERSION;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHelper";
    private static final String DATABASE_NAME = "fitnessBase.db";

    private static final String db_track_create =  "create table if not exists " + TrackTable.NAME + "(" +
            TrackTable.Cols.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            TrackTable.Cols.USER_ID + ", " +
            TrackTable.Cols.START_DATE + ", " +
            TrackTable.Cols.START_TIME + ", " +
            TrackTable.Cols.RUN_DISTANCE + ", " +
            TrackTable.Cols.RUN_TIME + ", " +
            TrackTable.Cols.SERVER_SYNC_TIMESTAMP + ", " +
            TrackTable.Cols.LIST_LOCATIONS + ")";

    private static final String db_notifications_create =  "create table if not exists " + NotificationTable.NAME + "(" +
            NotificationTable.Cols.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            NotificationTable.Cols.USER_ID + ", " +
            NotificationTable.Cols.TITLE + ", " +
            NotificationTable.Cols.DATE + ", " +
            NotificationTable.Cols.TIME + ")";

    private static final String db_track_drop = "DELETE FROM " + TrackTable.NAME;
    private static final String db_notifications_drop = "DELETE FROM " + NotificationTable.NAME;


    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(db_track_create);
        db.execSQL(db_notifications_create);
        Log.d(TAG, "DATABASE CREATED onCreate");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "Upgrading com.senla.fitnessimpulse.database from " + oldVersion + " to " + newVersion + " version");
        db.execSQL(db_track_drop);
        db.execSQL(db_notifications_drop);
        onCreate(db);
    }
}
