package com.senla.fitnessimpulse.database;

public class FitnessDbSchema {
    public static final class TrackTable {
        public static final String NAME = "tracks";
        public static final int DATABASE_VERSION = 1;

        public static final class Cols {
            public static final String ID = "id";
            public static final String USER_ID = "user_id";
            public static final String START_DATE = "start_date";
            public static final String START_TIME = "start_time";
            public static final String RUN_DISTANCE = "run_distance";
            public static final String RUN_TIME = "run_time";
            public static final String LIST_LOCATIONS = "list_locations";
            public static final String SERVER_SYNC_TIMESTAMP = "sync_timestamp";
        }
    }

    public static final class NotificationTable {
        public static final String NAME = "notifications";
        static final int DATABASE_VERSION = 1;

        public static final class Cols {
            public static final String ID = "id";
            public static final String POSITION = "position";
            public static final String USER_ID = "user_id";
            public static final String TITLE = "title";
            public static final String DATE = "date";
            public static final String TIME = "time";
        }
    }
}
