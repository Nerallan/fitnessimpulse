package com.senla.fitnessimpulse.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.senla.fitnessimpulse.PrefManager;
import com.senla.fitnessimpulse.R;
import com.senla.fitnessimpulse.activity.TimerActivity;
import com.senla.fitnessimpulse.adapter.BaseRecyclerViewAdapter;
import com.senla.fitnessimpulse.adapter.OnRecyclerItemClickListener;
import com.senla.fitnessimpulse.adapter.TrackRecyclerViewAdapter;
import com.senla.fitnessimpulse.model.Track;
import com.senla.fitnessimpulse.util.HttpSyncCallback;
import com.senla.fitnessimpulse.util.HttpTrackCallback;
import com.senla.fitnessimpulse.util.PermissionHandler;
import com.senla.fitnessimpulse.util.ServerTrackHttp;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class TrackListFragment extends BaseListFragment implements
        OnRecyclerItemClickListener,
        HttpTrackCallback,
        HttpSyncCallback {

    private static final String TAG = "TrackListFragment";
    private static final int TIMER_ACTIVITY_REQUEST_CODE = 1001;

    private DataToMapFragmentListener dataToMapFragmentListener;
    private TrackRecyclerViewAdapter trackRecyclerViewAdapter;
    private TrackDbListener trackDbListener;

    @Nullable
    private ProgressDialog progressDialog;

    public interface TrackDbListener{
        List<Track> getTracksFromDb();
        void updateTimestampInDb(List<Track> trackList);
        boolean insertServerDataToDb(List<Track> trackList);
    }

    public interface DataToMapFragmentListener{
        void passTrackIdToMap(long position);
    }

    public static Fragment newInstance(){
        return new TrackListFragment();
    }

    Comparator<Track> comparator = new Comparator<Track>() {
        @Override
        public int compare(Track t1, Track t2) {
            return getTimestampFromDate(t1.getStartDate(), t1.getStartTime()).compareTo(getTimestampFromDate(t2.getStartDate(), t2.getStartTime()));
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TrackDbListener){
            trackDbListener = (TrackDbListener) context;
        }
        if (context instanceof DataToMapFragmentListener){
            dataToMapFragmentListener = (DataToMapFragmentListener) context;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TIMER_ACTIVITY_REQUEST_CODE){
            if(resultCode == RESULT_OK){
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    protected int getLayout() {
        syncTracks();
        return R.layout.fragment_list;
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(getContext());
    }

    @Override
    protected BaseRecyclerViewAdapter getAdapter() {
        trackRecyclerViewAdapter = new TrackRecyclerViewAdapter(getActivity(), this);
        updateAdapterData();
        return trackRecyclerViewAdapter;
    }

    @Override
    protected void setSwipeToRefresh() {
        // Setup refresh listener which triggers new data loading
        if (swipeContainer != null) {
            swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    if(swipeContainer != null){
                        swipeContainer.setRefreshing(true);
                    }
                    syncDbWithServer();
                }
            });
            // Configure the refreshing colors
            swipeContainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        }
    }

    private void makeForceSynchronization(){
        PermissionHandler permissionHandler = new PermissionHandler();
        if (permissionHandler.isNetworkAvailable(getActivity())){
            ServerTrackHttp serverTrackHttp = new ServerTrackHttp();
            serverTrackHttp.setHttpTrackCallback(this);
            serverTrackHttp.executeGetTracksTask(PrefManager.getInstance(getActivity()).getUser().getUserId());
        } else {
            Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();
            if (swipeContainer != null) {
                swipeContainer.setRefreshing(false);
            }
        }
    }

    private Date getTimestampFromDate(String date, String time){
        String timeString = date + " " + time;
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm", Locale.getDefault());
        Date dateObject = null;
        try {
            dateObject = formatter.parse(timeString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateObject;
    }

    private void syncTracks(){
        PrefManager prefManager = PrefManager.getInstance(getActivity());
        if (prefManager.getUser().getUserId() != null){
            if(prefManager.isFirstTimeLaunch()){
                showProgressDialog();
                makeForceSynchronization();
            } else {
                syncDbWithServer();
            }
            prefManager.setFirstTimeLaunch(false);
        }
    }

    protected void showProgressDialog(){
        progressDialog = new ProgressDialog(getActivity(), R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Get tracks from server...");
        progressDialog.show();
    }

    private void syncDbWithServer() {
        PermissionHandler permissionHandler = new PermissionHandler();
        if (permissionHandler.isNetworkAvailable(getActivity())){
            ServerTrackHttp serverTrackHttp = new ServerTrackHttp();
            serverTrackHttp.setHttpSyncCallback(this);
            serverTrackHttp.executeDbWithServerSyncTask(PrefManager.getInstance(getActivity()).getUser().getUserId(), trackDbListener.getTracksFromDb());
        } else {
            Toast.makeText(getActivity(), "No internet connection", Toast.LENGTH_SHORT).show();
            if (swipeContainer != null) {
                swipeContainer.setRefreshing(false);
            }
        }
    }

    private void updateAdapterData() {
        List<Track> tracks = trackDbListener.getTracksFromDb();
        sortAndReverseList(tracks, comparator);
        trackRecyclerViewAdapter.setItems(tracks);
    }

    private void sortAndReverseList(List<Track> trackList, Comparator<Track> comparator){
        Collections.sort(trackList, comparator);
        Collections.reverse(trackList);
    }

    @Override
    public void onFailure(Throwable throwable) {
        Log.d(TAG, "onFailure: " + throwable.getMessage());
        if (swipeContainer != null) {
            swipeContainer.setRefreshing(false);
        }
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onSuccess(List<Track> trackList) {
        Log.d(TAG, "onSuccess: " + trackList.toString());
        if (swipeContainer != null) {
            swipeContainer.setRefreshing(false);
        }
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        if(trackDbListener.insertServerDataToDb(trackList)){
            updateAdapterData();
        }
    }

    @Override
    public void onSyncFailure(Throwable throwable) {
        Log.d(TAG, "onSyncFailure: " + throwable.getMessage());
        if (swipeContainer != null) {
            swipeContainer.setRefreshing(false);
        }
    }

    @Override
    public void onSyncSuccess(List<Track> result) {
        Log.d(TAG, "onSyncSuccess: ");
        if (swipeContainer != null) {
            swipeContainer.setRefreshing(false);
        }
        trackDbListener.updateTimestampInDb(result);
    }

    // fab click
    @Override
    public void onClick(View v) {
        Intent timerActivityIntent = new Intent(getActivity(), TimerActivity.class);
        startActivityForResult(timerActivityIntent, TIMER_ACTIVITY_REQUEST_CODE);
    }

    @Override
    public void onItemClick(int position) {
        List<Track> trackList = trackDbListener.getTracksFromDb();
        sortAndReverseList(trackList, comparator);
        dataToMapFragmentListener.passTrackIdToMap(trackList.get(position).getId());
    }
}
