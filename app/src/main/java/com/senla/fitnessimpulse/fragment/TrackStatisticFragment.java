package com.senla.fitnessimpulse.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.senla.fitnessimpulse.R;

public class TrackStatisticFragment extends Fragment {

    private static final String ARGS_STATISTIC_TIME = "com.senla.fitnessimpulse.fragment.STATISTIC_TIME";
    private static final String ARGS_STATISTIC_DISTANCE = "com.senla.fitnessimpulse.fragment.STATISTIC_DISTANCE";

    @Nullable
    private TextView timerStatisticTextView;
    @Nullable
    private TextView distanceStatisticTextView;

    public static TrackStatisticFragment newInstance(){
        return new TrackStatisticFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_track_statistic, container, false);
        initViews(view);

        String time = null;
        String distance = null;
        if (getArguments() != null) {
            time = getArguments().getString(ARGS_STATISTIC_TIME);
            distance = getArguments().getString(ARGS_STATISTIC_DISTANCE);
        }
        if (timerStatisticTextView != null) {
            timerStatisticTextView.setText(time);
        }
        if (distanceStatisticTextView != null) {
            distanceStatisticTextView.setText(distance);
        }
        return view;
    }

    private void initViews(View view) {
        timerStatisticTextView = view.findViewById(R.id.timer_statistic_text_view);
        distanceStatisticTextView = view.findViewById(R.id.distance_statistic_text_view);
    }
}

