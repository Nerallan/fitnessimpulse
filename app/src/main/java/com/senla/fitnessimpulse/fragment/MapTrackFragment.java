package com.senla.fitnessimpulse.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Dot;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.JointType;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;
import com.senla.fitnessimpulse.R;
import com.senla.fitnessimpulse.model.Location;
import com.senla.fitnessimpulse.model.Track;
import com.senla.fitnessimpulse.util.PermissionHandler;

import java.util.Arrays;
import java.util.List;

public class MapTrackFragment extends Fragment implements
        OnMapReadyCallback,
        GoogleMap.OnPolylineClickListener{

    private static final String TAG = "MapTrackFragment";
    private static final String ARGS_MAP_TRACK_POSITION = "com.senla.fitnessimpulse.fragment.MAP_POSITION";

    private static final int COLOR_BLACK_ARGB = 0xff000000;

    private static final int POLYLINE_STROKE_WIDTH_PX = 12;
    private static final int PATTERN_DASH_LENGTH_PX = 20;
    private static final int PATTERN_GAP_LENGTH_PX = 20;
    private static final PatternItem DOT = new Dot();
    private static final PatternItem DASH = new Dash(PATTERN_DASH_LENGTH_PX);
    private static final PatternItem GAP = new Gap(PATTERN_GAP_LENGTH_PX);

    // Create a stroke pattern of a gap followed by a dot.
    private static final List<PatternItem> PATTERN_POLYLINE_DOTTED = Arrays.asList(GAP, DOT);

    // Create a stroke pattern of a gap followed by a dash.
    private static final List<PatternItem> PATTERN_POLYGON_ALPHA = Arrays.asList(GAP, DASH);

    // Create a stroke pattern of a dot followed by a gap, a dash, and another gap.
    private static final List<PatternItem> PATTERN_POLYGON_BETA =
            Arrays.asList(DOT, GAP, DASH, GAP);
    private Track track;
    private long id;
    private SupportMapFragment mapFragment;
    private TrackIdListener trackDataListener;

    @Nullable
    private TextView runTimeTextView;
    @Nullable
    private TextView distanceTextView;
    @Nullable
    private AlertDialog errorAlertDialog;

    public interface TrackIdListener{
        Track getTrackById(long position);
    }

    public static MapTrackFragment newInstance(){
        return new MapTrackFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof TrackIdListener){
            trackDataListener = (TrackIdListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map_track, container, false);
        initViews(view);
        checkInternetConnection();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get the SupportMapFragment and request notification when the map is ready to be used.
        if(getActivity() != null){
            mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
            if (mapFragment != null) {
                mapFragment.getMapAsync(this);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
        Bundle args = getArguments();
        if (args != null){
            id = args.getLong(ARGS_MAP_TRACK_POSITION);
            track = trackDataListener.getTrackById(id);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
        if (runTimeTextView != null){
            runTimeTextView.setText(track.getRunTime());
        }
        if (distanceTextView != null){
            distanceTextView.setText(track.getRunDistance());
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        // Add polylines to the map.
        // Polylines are useful to show a route or some other connection between points.
        Log.d(TAG, "onMapReady: ");
        PolylineOptions polylineOptions = new PolylineOptions().clickable(true);
        if(!track.getLocationList().isEmpty()){
            for (Location location : track.getLocationList()){
                polylineOptions.add(new LatLng(location.getLatitude(), location.getLongitude()));
            }
            Polyline polyline = googleMap.addPolyline(polylineOptions);

            addMarkers(googleMap, track.getLocationList().get(0), track.getLocationList().get(track.getLocationList().size() - 1));

            // Store a data object with the polyline, used here to indicate an arbitrary type.
            polyline.setTag("A");
            // Style the polyline.
            stylePolyline(polyline);
            zoomRoute(googleMap, track.getLocationList());
            // Set listeners for click events.
            googleMap.setOnPolylineClickListener(this);
        } else {
            showErrorAlertDialog("Locations for track not found");
        }
    }

    private void checkInternetConnection() {
        PermissionHandler permissionHandler = new PermissionHandler();
        if (!permissionHandler.isNetworkAvailable(getActivity())){
            showErrorAlertDialog("Please, turn on Internet...");
        }
    }

    private void initViews(View view) {
        runTimeTextView = view.findViewById(R.id.run_time_text_view);
        distanceTextView = view.findViewById(R.id.distance_text_view);
    }

    private void showErrorAlertDialog(String message) {
        if(getActivity() != null){
            errorAlertDialog = new AlertDialog.Builder(getActivity())
                    .setTitle("Error occurred")
                    .setMessage(message)
                    .show();
            errorAlertDialog.setIcon(android.R.drawable.ic_dialog_alert);
        }
    }

    private void addMarkers(GoogleMap googleMap, Location startLocation, Location finishLocation) {
        // Creating a marker
        MarkerOptions startMarkerOptions = new MarkerOptions();
        MarkerOptions finishMarkerOptions = new MarkerOptions();

        // Setting the position for the marker
        setMarkerPosition(startMarkerOptions, startLocation);
        setMarkerPosition(finishMarkerOptions, finishLocation);

        // change markers color
        startMarkerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        finishMarkerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

        // Setting the title for the marker.
        // This will be displayed on taping the marker
        startMarkerOptions.title("Start");
        finishMarkerOptions.title("Finish");
        googleMap.addMarker(startMarkerOptions);
        googleMap.addMarker(finishMarkerOptions);
    }

    private void setMarkerPosition(MarkerOptions targetMarkerOptions, Location location){
        targetMarkerOptions.position(new LatLng(location.getLatitude(), location.getLongitude()));
    }

    private void zoomRoute(GoogleMap googleMap, List<Location> locationList){
        if (googleMap != null && locationList != null){
            LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
            for (Location location : locationList){
                LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                boundsBuilder.include(latLng);
            }

            // TODO: using animation or moveCamera with width and height box boundaries
//            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, routePadding));
            int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.7);
            int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.7);
            int routePadding = (int) (width * 0.2); // offset from edges of the map 15% of screen
            // use this method at any time, even before the map has undergone layout, because the API calculates the display boundaries from the arguments that you pass.
            LatLngBounds latLngBounds = boundsBuilder.build();
            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, width, height, routePadding));
        }
    }

    /**
     * Styles the polyline, based on type.
     * @param polyline The polyline object that needs styling.
     */
    private void stylePolyline(Polyline polyline) {
        String type = "";
        // Get the data object stored with the polyline.
        if (polyline.getTag() != null) {
            type = polyline.getTag().toString();
        }
        // Use a round cap at the start of the line.
        polyline.setStartCap(new RoundCap());
        polyline.setEndCap(new RoundCap());
        polyline.setWidth(POLYLINE_STROKE_WIDTH_PX);
        polyline.setColor(COLOR_BLACK_ARGB);
        polyline.setJointType(JointType.ROUND);
    }

    @Override
    public void onPolylineClick(Polyline polyline) {
        // Flip from solid stroke to dotted stroke pattern.
        if ((polyline.getPattern() == null) || (!polyline.getPattern().contains(DOT))) {
            polyline.setPattern(PATTERN_POLYLINE_DOTTED);
        } else {
            // The default pattern is a solid stroke.
            polyline.setPattern(null);
        }
        if (polyline.getTag() != null){
            Toast.makeText(getActivity(), "Route type " + polyline.getTag().toString(),
                    Toast.LENGTH_SHORT).show();
        }
    }
}
