package com.senla.fitnessimpulse.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.senla.fitnessimpulse.PrefManager;
import com.senla.fitnessimpulse.R;
import com.senla.fitnessimpulse.model.Location;
import com.senla.fitnessimpulse.model.Track;
import com.senla.fitnessimpulse.util.GpsLocationService;
import com.senla.fitnessimpulse.util.PermissionHandler;
import com.senla.fitnessimpulse.util.ServerTrackHttp;
import com.senla.fitnessimpulse.util.StopWatchService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LaunchedTimerFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "LaunchedTimerFragment";
    private static final String ARGS_START_DATE = "com.senla.fitnessimpulse.fragment.START_DATE";
    private static final String ARGS_START_TIME = "com.senla.fitnessimpulse.fragment.START_TIME";

    private static final String KEY_TIME = "com.senla.fitnessimpulse.fragment.TIME";
    private static final String KEY_LOCATION = "com.senla.fitnessimpulse.fragment.LOCATION";

    @Nullable
    private Button finishTimerButton;
    @Nullable
    private TextView timerTextView;
    @Nullable
    private AlertDialog errorAlertDialog;

    private Intent timerIntent;
    private Intent locationIntent;

    private String startDate;
    private String startTime;
    private String runTime;

    private View constraintLayout;
    private List<Location> locationList;
    private GpsLocationService gpsLocationService;
    private DataToStatisticListener dataToStatisticListener;
    private AddToDbListener addToDbListener;
    private LastDbItemListener lastDbItemListener;

    public static LaunchedTimerFragment newInstance() {
        return new LaunchedTimerFragment();
    }

    public interface DataToStatisticListener{
        void passDataToStatisticFragment(String runTime, String runDistance);
    }

    public interface AddToDbListener{
        void addTrackToDb(Track track);
    }

    public interface LastDbItemListener{
        Track getLastTrackFromDb();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DataToStatisticListener){
            dataToStatisticListener = (DataToStatisticListener) context;
        }
        if (context instanceof AddToDbListener){
            addToDbListener = (AddToDbListener) context;
        }
        if (context instanceof LastDbItemListener){
            lastDbItemListener = (LastDbItemListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_launched_timer, container, false);
        Log.d(TAG, "onCreateView: ");
        initViews(view);
        if (finishTimerButton != null) {
            finishTimerButton.setOnClickListener(this);
        }
        locationList = new ArrayList<>();
        if(getActivity() != null){
            gpsLocationService = new GpsLocationService(getActivity());
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
        Bundle args = getArguments();
        if (args != null){
            startDate = args.getString(ARGS_START_DATE);
            startTime = args.getString(ARGS_START_TIME);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: ");
        timerIntent = new Intent(getActivity(), StopWatchService.class);
        locationIntent = new Intent(getActivity(), GpsLocationService.class);
        startServices();
        registerReceivers();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause: ");
        unregisterReceivers();
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop: ");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d(TAG, "onDestroyView: ");
    }

    private void initViews(View view) {
        constraintLayout = view.findViewById(R.id.launched_constraint_layout);
        finishTimerButton = view.findViewById(R.id.finish_timer_button);
        timerTextView = view.findViewById(R.id.timer_text_view);
    }

    private BroadcastReceiver timerBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        updateTimer(intent);
        }
    };

    private BroadcastReceiver locationBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        updateLocation(intent);
        }
    };

    private float getGeneralDistance(){
        float generalDistance = 0;
        for (int index = 0; index < locationList.size() - 1; index++){
            generalDistance += getDistanceBwGeoPoints(locationList.get(index), locationList.get(index+1));
        }
        return generalDistance;
    }

    private float getDistanceBwGeoPoints(Location startCustomLocation, Location finishCustomLocation){
        android.location.Location locationStart = new android.location.Location("");
        locationStart.setLatitude(startCustomLocation.getLatitude());
        locationStart.setLongitude(startCustomLocation.getLongitude());

        android.location.Location locationFinish = new android.location.Location("");
        locationFinish.setLatitude(finishCustomLocation.getLatitude());
        locationFinish.setLongitude(finishCustomLocation.getLongitude());
        return locationStart.distanceTo(locationFinish);
    }

    private void updateLocation(Intent intent) {
        if (intent.getExtras() != null){
            android.location.Location newLocation = intent.getExtras().getParcelable(KEY_LOCATION);
            if (newLocation != null){
                locationList.add(new Location(newLocation.getLatitude(), newLocation.getLongitude()));
            }
        }
    }

    private void updateTimer(Intent intent) {
        String time = intent.getStringExtra(KEY_TIME);
        Log.d(TAG, time);
        if (timerTextView != null) {
            timerTextView.setText(time);
        }
    }

    private void startServices(){
        if (getActivity() != null){
            getActivity().startService(timerIntent);
            getActivity().startService(locationIntent);
//            getActivity().startForegroundService(intent);
        }
    }

    private void registerReceivers(){
        if (getActivity() != null){
            getActivity().registerReceiver(timerBroadcastReceiver, new IntentFilter(StopWatchService.ACTION_STOPWATCH));
            getActivity().registerReceiver(locationBroadcastReceiver, new IntentFilter(GpsLocationService.ACTION_LOCATION));
        }
    }

    private void stopServices() {
        if(getActivity() != null) {
            getActivity().stopService(timerIntent);
            getActivity().stopService(locationIntent);
        }
    }

    private void unregisterReceivers(){
        if(getActivity() != null){
            getActivity().unregisterReceiver(timerBroadcastReceiver);
            getActivity().unregisterReceiver(locationBroadcastReceiver);
        }
    }

    private void addToServer(Track track){
        // TODO: add track to server
        ServerTrackHttp serverTrackHttp = new ServerTrackHttp();
        serverTrackHttp.executeAddTrackTask(PrefManager.getInstance(getActivity()).getUser().getUserId(), track);
    }

    private void showErrorAlertDialog() {
        if (getActivity() != null){
            errorAlertDialog = new AlertDialog.Builder(getActivity())
                .setTitle("Error occurred")
                .setMessage("Please, turn on Internet...")
                .show();
            errorAlertDialog.setIcon(android.R.drawable.ic_dialog_alert);
        }
    }

    private Track populateTrackModel() {
        return new Track(startDate, startTime, String.valueOf(getGeneralDistance()), runTime, locationList, new Date().getTime());
    }

    @Override
    public void onClick(View v) {
        if (timerTextView != null) {
            runTime = timerTextView.getText().toString();
        }
        stopServices();
        Track track = populateTrackModel();
        dataToStatisticListener.passDataToStatisticFragment(track.getRunTime(), track.getRunDistance());

        addToDbListener.addTrackToDb(track);
        // get new track from db, to set id_in_db value
        Track lastTrack = lastDbItemListener.getLastTrackFromDb();
        PermissionHandler permissionHandler = new PermissionHandler();
        if(permissionHandler.isNetworkAvailable(getActivity())){
            addToServer(lastTrack);
        } else {
            showErrorAlertDialog();
        }
    }
}
