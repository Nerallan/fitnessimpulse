package com.senla.fitnessimpulse.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;

import com.senla.fitnessimpulse.R;
import com.senla.fitnessimpulse.adapter.BaseRecyclerViewAdapter;
import com.senla.fitnessimpulse.adapter.NotificationRecyclerViewAdapter;
import com.senla.fitnessimpulse.adapter.OnRecyclerItemClickListener;
import com.senla.fitnessimpulse.model.Notification;

import java.util.List;

public class NotificationListFragment extends BaseListFragment implements OnRecyclerItemClickListener{

    private static final String ARGS_NOTIFICATION_ID = "com.senla.fitnessimpulse.fragment.NOTIFICATION_ID";
    private static final String NOTIFICATION_CHANGE_FRAGMENT = "com.senla.fitnessimpulse.fragment.NotificationChangeFragment";
    private static final String TAG = "NotificationListFragment";

    private NotificationListDBListener notificationListDBListener;
    private NotificationRecyclerViewAdapter notificationRecyclerViewAdapter;


    public interface NotificationListDBListener{
        List<Notification> getNotificationsFromDb();
        void deleteNotificationById(long position);
    }

    public static NotificationListFragment newInstance(){
        return new NotificationListFragment();
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_list;
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(getContext());
    }

    @Override
    protected BaseRecyclerViewAdapter getAdapter() {
        notificationRecyclerViewAdapter = new NotificationRecyclerViewAdapter(getActivity(), this);
        // get recycleView instance from parent class
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchHelperCallback);
        itemTouchHelper.attachToRecyclerView(super.recyclerView);
        notificationRecyclerViewAdapter.setItems(notificationListDBListener.getNotificationsFromDb());
        return notificationRecyclerViewAdapter;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach: ");
        if (context instanceof NotificationListDBListener){
            notificationListDBListener = (NotificationListDBListener) context;
        }
    }

    private void startNotificationChangeFragment(long position){
        NotificationChangeFragment notificationChangeFragment = NotificationChangeFragment.newInstance();
        Bundle args = new Bundle();
        args.putLong(ARGS_NOTIFICATION_ID, position);
        notificationChangeFragment.setArguments(args);
        if(getActivity() != null){
            getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, notificationChangeFragment, NOTIFICATION_CHANGE_FRAGMENT)
                .addToBackStack(null)
                .commit();
        }
    }

    ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
            // first delete from adapter then from db
            int position = viewHolder.getAdapterPosition();
            notificationRecyclerViewAdapter.remove(position);
            long positionForDb = notificationListDBListener.getNotificationsFromDb().get(position).getId();
            notificationListDBListener.deleteNotificationById(positionForDb);
        }
    };

    @Override
    protected void setSwipeToRefresh() {
        if (swipeContainer != null) {
            swipeContainer.setEnabled(false);
        }
    }

    // fab click
    @Override
    public void onClick(View v) {
        int newNotification = -1;
        startNotificationChangeFragment(newNotification);
    }

    @Override
    public void onItemClick(int position) {
        // get the User entity, associated with the clicked item.
        final Notification clickedNotification = notificationRecyclerViewAdapter.getData(position);
        // now you are free to do whatever you want with it.
        // We just show a Toast message

//        Toast.makeText(getActivity(), clickedNotification.getTitle(), Toast.LENGTH_SHORT).show();
        long positionForDb = notificationListDBListener.getNotificationsFromDb().get(position).getId();
        startNotificationChangeFragment(positionForDb);
    }
}
