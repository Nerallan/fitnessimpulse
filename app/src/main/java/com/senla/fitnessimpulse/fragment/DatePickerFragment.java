package com.senla.fitnessimpulse.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;

import com.senla.fitnessimpulse.R;

import java.util.Calendar;
import java.util.Objects;

public class DatePickerFragment extends DialogFragment {
    private static final String NOTIFICATION_CHANGE_FRAGMENT = "com.senla.fitnessimpulse.fragment.NotificationChangeFragment";
    private static final String ARGS_NOTIFICATION_YEAR = "com.senla.fitnessimpulse.fragment.ARGS_NOTIFICATION_YEAR";
    private static final String ARGS_NOTIFICATION_MONTH = "com.senla.fitnessimpulse.fragment.ARGS_NOTIFICATION_MONTH";
    private static final String ARGS_NOTIFICATION_DAY = "com.senla.fitnessimpulse.fragment.ARGS_NOTIFICATION_DAY";

    public static DatePickerFragment newInstance(){
        return new DatePickerFragment();
    }

    public static DatePickerFragment newInstance(int year, int month, int day){
        Bundle args = new Bundle();
        args.putInt(ARGS_NOTIFICATION_YEAR, year);
        args.putInt(ARGS_NOTIFICATION_MONTH, month);
        args.putInt(ARGS_NOTIFICATION_DAY, day);
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();
        int year;
        int month;
        int day;
        if (getArguments() != null){
            year = getArguments().getInt(ARGS_NOTIFICATION_YEAR);
            month = getArguments().getInt(ARGS_NOTIFICATION_MONTH);
            day = getArguments().getInt(ARGS_NOTIFICATION_DAY);
        } else {
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
        }

        return new DatePickerDialog(getActivity(), R.style.DialogTheme,
                (DatePickerDialog.OnDateSetListener) Objects.requireNonNull(getActivity()).getSupportFragmentManager().findFragmentByTag(NOTIFICATION_CHANGE_FRAGMENT),
                year,
                month,
                day);
    }
}
