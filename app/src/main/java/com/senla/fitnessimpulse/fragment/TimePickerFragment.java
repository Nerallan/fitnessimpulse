package com.senla.fitnessimpulse.fragment;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;

import com.senla.fitnessimpulse.R;

import java.util.Calendar;
import java.util.Objects;

public class TimePickerFragment extends DialogFragment {

    private static final String NOTIFICATION_CHANGE_FRAGMENT = "com.senla.fitnessimpulse.fragment.NotificationChangeFragment";
    private static final String ARGS_NOTIFICATION_HOUR = "com.senla.fitnessimpulse.fragment.ARGS_NOTIFICATION_HOUR";
    private static final String ARGS_NOTIFICATION_MINUTE = "com.senla.fitnessimpulse.fragment.ARGS_NOTIFICATION_MINUTE";

    public static TimePickerFragment newInstance(){
        return new TimePickerFragment();
    }

    public static TimePickerFragment newInstance(int hour, int minute){
        Bundle args = new Bundle();
        args.putInt(ARGS_NOTIFICATION_HOUR, hour);
        args.putInt(ARGS_NOTIFICATION_MINUTE, minute);
        TimePickerFragment fragment = new TimePickerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();
        int hour;
        int minute;
        if (getArguments() != null){
            hour = getArguments().getInt(ARGS_NOTIFICATION_HOUR);
            minute = getArguments().getInt(ARGS_NOTIFICATION_MINUTE);
        } else {
            hour = calendar.get(Calendar.HOUR_OF_DAY);
            minute = calendar.get(Calendar.MINUTE);
        }
        return new TimePickerDialog(getActivity(), R.style.DialogTheme,
                (TimePickerDialog.OnTimeSetListener) Objects.requireNonNull(getActivity()).getSupportFragmentManager().findFragmentByTag(NOTIFICATION_CHANGE_FRAGMENT),
                hour,
                minute,
                DateFormat.is24HourFormat(getActivity()));
    }
}
