package com.senla.fitnessimpulse.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.senla.fitnessimpulse.R;
import com.senla.fitnessimpulse.TrackItemDecoration;
import com.senla.fitnessimpulse.adapter.BaseRecyclerViewAdapter;

import static android.app.Activity.RESULT_OK;

public abstract class BaseListFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "BaseListFragment";

    @Nullable
    protected RecyclerView recyclerView;
    @Nullable
    private FloatingActionButton floatingActionButton;
    @Nullable
    protected SwipeRefreshLayout swipeContainer;
    @NonNull
    private BaseRecyclerViewAdapter adapter;

    private OnFabClickListener onFabClickListener;

    public interface OnFabClickListener{
        void onClickFab();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach: ");
        if(context instanceof OnFabClickListener){
            onFabClickListener = (OnFabClickListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(getLayout(), container, false);
        initViews(rootView);
        if (floatingActionButton != null) {
            floatingActionButton.setOnClickListener(this);
        }
        swipeContainer = rootView.findViewById(R.id.swipe_to_refresh_container);
        setSwipeToRefresh();
        setupRecyclerView();
        setCollection();
        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            setCollection();
        }
    }

    private void initViews(View rootView) {
        recyclerView =  rootView.findViewById(R.id.recycler_view_main);
        floatingActionButton = rootView.findViewById(R.id.floating_action_button);
    }

    private void setupRecyclerView() {
        if(getLayoutManager() != null && recyclerView != null) {
            recyclerView.setLayoutManager(getLayoutManager());
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            if (getActivity() != null){
                recyclerView.addItemDecoration(new TrackItemDecoration(getActivity()));
            }
        }
    }

    private void setCollection(){
        adapter = getAdapter();
        if (recyclerView != null) {
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void onClick(View v) {
        onFabClickListener.onClickFab();
    }

    protected abstract void setSwipeToRefresh();

    protected abstract int getLayout();

    protected abstract RecyclerView.LayoutManager getLayoutManager();

    protected abstract BaseRecyclerViewAdapter getAdapter();
}
