package com.senla.fitnessimpulse.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.senla.fitnessimpulse.R;
import com.senla.fitnessimpulse.util.GpsLocationService;
import com.senla.fitnessimpulse.util.PermissionHandler;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class StartTimerFragment extends Fragment implements View.OnClickListener{
    private static final int REQUEST_PERMISSIONS_CODE = 1;

    private DataPassToLaunchListener passListener;

    @Nullable
    private Button startTimerButton;

    public interface DataPassToLaunchListener{
        void passDataToLaunchFragment(String startDate, String startTime);
    }

    public static StartTimerFragment newInstance(){
        return new StartTimerFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start_timer, container, false);
        initViews(view);
        if (startTimerButton != null) {
            startTimerButton.setOnClickListener(this);
        }
        if(getActivity() != null){
            PermissionHandler permissionHandler = new PermissionHandler();
            if(permissionHandler.isLocationPermissionGranted(getActivity())){
                ckeckGpsStatus();
            } else {
                permissionHandler.requestLocationPermission(this);
            }
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof DataPassToLaunchListener){
            passListener = (DataPassToLaunchListener) context;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode){
            case REQUEST_PERMISSIONS_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    if (getActivity() != null){
                        ckeckGpsStatus();
                    }
                }
        }
    }

    private void initViews(View view) {
        startTimerButton = view.findViewById(R.id.start_timer_button);
    }

    private void ckeckGpsStatus() {
        if (getActivity() != null){
            final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            if (manager != null && !manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                buildAlertMessageNoGps();
            }
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private String getCurrentDate(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        return simpleDateFormat.format(calendar.getTime());
    }

    private String getCurrentTime(){
        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm", Locale.getDefault());
        return simpleDateFormat.format(currentTime);
    }

    @Override
    public void onClick(View v) {
        if (passListener != null){
            passListener.passDataToLaunchFragment(getCurrentDate(), getCurrentTime());
        }
    }
}
