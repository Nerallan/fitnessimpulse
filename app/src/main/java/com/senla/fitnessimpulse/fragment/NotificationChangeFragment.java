package com.senla.fitnessimpulse.fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.senla.fitnessimpulse.R;
import com.senla.fitnessimpulse.model.Notification;
import com.senla.fitnessimpulse.util.AlarmReceiver;
import com.senla.fitnessimpulse.util.NotificationScheduler;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class NotificationChangeFragment extends Fragment implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {
    private static final String ARGS_NOTIFICATION_ID = "com.senla.fitnessimpulse.fragment.NOTIFICATION_ID";
    private static final String TAG = "NotChangeFragment";

    @Nullable
    private TextInputLayout titleEditLayout;
    @Nullable
    private EditText titleEditText;
    @Nullable
    private TextView dateTextView;
    @Nullable
    private TextView timeTextView;
    @Nullable
    private Button datePickerButton;
    @Nullable
    private Button timePickerButton;
    @Nullable
    private Button okButton;
    @Nullable
    private Notification notification;
    @Nullable
    private DialogFragment timePickerDialog;
    @Nullable
    private DialogFragment datePickerDialog;

    private long notificationId;

    private NotificationDbListener notificationDataListener;

    public interface NotificationDbListener{
        Notification getNotificationById(long position);
        void addNotificationToDb(Notification notification);
        void updateNotificationInDb(Notification notification);
    }

    public static NotificationChangeFragment newInstance(){
        return new NotificationChangeFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof NotificationDbListener){
            notificationDataListener = (NotificationDbListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification_change, container, false);
        initViews(view);
        if(getArguments() != null) {
            notificationId = getArguments().getLong(ARGS_NOTIFICATION_ID);
        }
        // edit old pickers
        if (notificationId != -1){
            showOldNotification(notificationId);
        } else {
            // start clear pickers
            // fab was clicked
        }
        return view;
    }

    private void initViews(View view) {
        titleEditLayout = view.findViewById(R.id.til_notification_title);
        titleEditText = view.findViewById(R.id.notification_title_edit_text);
        dateTextView = view.findViewById(R.id.notification_date_text_view);
        timeTextView = view.findViewById(R.id.notification_time_text_view);
        datePickerButton = view.findViewById(R.id.date_picker_button);
        timePickerButton = view.findViewById(R.id.time_picker_button);
        okButton = view.findViewById(R.id.notification_confirm_button);
        if (datePickerButton != null) {
            datePickerButton.setOnClickListener(v -> {
                showDatePickerDialog();
            });
        }
        if (timePickerButton != null) {
            timePickerButton.setOnClickListener(v -> {
                showTimePickerDialog();
            });
        }
        if(okButton != null) {
            okButton.setOnClickListener(v -> {
                if (isValidNotification()){
                    Notification notification = populateNotificationModel();
                    if(notificationId != -1){
                        notification.setId(notificationId);
                        notificationDataListener.updateNotificationInDb(notification);
                    } else {
                        notificationDataListener.addNotificationToDb(notification);
                    }
                    if (timeTextView != null && dateTextView != null){
                        addSchedulerNotification();
                    }
                    closeKeyboard();
                    if (getActivity() != null){
                        // transition back to NotificationList
                        getActivity().getSupportFragmentManager().popBackStackImmediate();
                    }
                }
            });
        }
    }

    private void closeKeyboard() {
        if (getActivity() != null){
            View view = getActivity().getCurrentFocus();
            if (view != null){
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    private boolean isValidNotification() {
        boolean valid = true;
        if(titleEditText != null) {
            String title = titleEditText.getText().toString();
            if(title.isEmpty()){
                if (titleEditLayout != null) {
                    titleEditLayout.setError(getText(R.string.notification_title_error));
                }
                valid = false;
            } else {
                if(titleEditLayout != null){
                    titleEditLayout.setError(null);
                }
            }
        }
        if (dateTextView != null){
            if (dateTextView.getText().toString().equals("Selected date")){
                Toast.makeText(getActivity(), "Choose date", Toast.LENGTH_SHORT).show();
                valid = false;
            }
        }
        if(timeTextView != null){
            if (timeTextView.getText().toString().equals("Selected time")){
                Toast.makeText(getActivity(), "Choose time", Toast.LENGTH_SHORT).show();
                valid = false;
            }
        }
        return valid;
    }

    private void addSchedulerNotification() {
        NotificationScheduler notificationScheduler = new NotificationScheduler();
        String title = null;
        if (titleEditText != null){
            title = titleEditText.getText().toString();
        }
        String time = null;
        if (timeTextView != null) {
            time = timeTextView.getText().toString();
        }
        String date = null;
        if (dateTextView != null) {
            date = dateTextView.getText().toString();
        }
        Calendar timeCalendar = timeStringToCalendar(time);
        Calendar dateCalendar = dateStringToCalendar(date);
        int year = dateCalendar.get(Calendar.YEAR);
        int month = dateCalendar.get(Calendar.MONTH);
        int day = dateCalendar.get(Calendar.DAY_OF_MONTH);
        int hour = timeCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = timeCalendar.get(Calendar.MINUTE);
        notificationScheduler.setNotification(getActivity(), AlarmReceiver.class, title, year, month, day, hour, minute);
    }

    private void showOldNotification(Long id) {
        notification = notificationDataListener.getNotificationById(id);
        if (titleEditText != null){
            titleEditText.setText(notification.getTitle());
        }
        if (dateTextView != null){
            dateTextView.setText(notification.getDate());
        }
        if (timeTextView != null){
            timeTextView.setText(notification.getTime());
        }
    }

    private Calendar timeStringToCalendar(String time){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        try {
            calendar.setTime(simpleDateFormat.parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar;
    }

    private Calendar dateStringToCalendar(String date){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault());
        try {
            calendar.setTime(simpleDateFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar;
    }

    private void showTimePickerDialog(){
        if (notification != null) {
            Calendar calendar = timeStringToCalendar(notification.getTime());
            int hour = calendar.get(Calendar.HOUR_OF_DAY);
            int minute = calendar.get(Calendar.MINUTE);
            timePickerDialog = TimePickerFragment.newInstance(hour, minute);
        } else {
            timePickerDialog = TimePickerFragment.newInstance();
        }
        if (timePickerDialog != null) {
            timePickerDialog.show(getChildFragmentManager(), "Time picker");
        }
    }

    private void showDatePickerDialog(){
        if (notification != null){
            Calendar calendar = dateStringToCalendar(notification.getDate());
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);
            datePickerDialog = DatePickerFragment.newInstance(year, month, day);
        } else {
            datePickerDialog = DatePickerFragment.newInstance();
        }
        if (datePickerDialog != null) {
            datePickerDialog.show(getChildFragmentManager(), "Date picker");
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        if (timeTextView != null) {
            timeTextView.setText(hourOfDay + ":" + minute);
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        if (dateTextView != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String selectedDate = DateFormat.getDateInstance().format(calendar.getTime());
            dateTextView.setText(selectedDate);
        }
    }

    private Notification populateNotificationModel() {
        String title = null;
        String date = null;
        String time = null;
        if (titleEditText != null) {
            title = titleEditText.getText().toString();
        }
        if (dateTextView != null){
            date = dateTextView.getText().toString();
        }
        if (timeTextView != null){
            time = timeTextView.getText().toString();
        }
        return new Notification(title, date, time);
    }
}
