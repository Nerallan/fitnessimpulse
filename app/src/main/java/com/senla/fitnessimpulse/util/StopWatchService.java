package com.senla.fitnessimpulse.util;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class StopWatchService extends Service {
    private static final String TAG = "StopWatchService";
    private static final String KEY_TIME = "com.senla.fitnessimpulse.fragment.TIME";
    public static final String ACTION_STOPWATCH = StopWatchService.class.getName();

    private long init;
    private long now;
    private long timeInMills;
    private long paused;
    private Intent intent;
    final Handler handler = new Handler();

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            displayTimer();
            handler.postDelayed(this, 50);
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: ");
        Toast.makeText(this, "Service timer created",
                Toast.LENGTH_SHORT).show();
        intent = new Intent(ACTION_STOPWATCH);
        init += System.currentTimeMillis();
        startTimer();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Put your timer code here
        Log.d(TAG, "onStartCommand: ");
        Toast.makeText(this, "Service timer running",
                Toast.LENGTH_SHORT).show();
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(runnable);
        runnable = null;
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
        Toast.makeText(this, "Service timer destroyed",
                Toast.LENGTH_SHORT).show();
    }

    private void startTimer() {
        handler.post(runnable);
    }

    private void displayTimer(){
        now =  System.currentTimeMillis();
        timeInMills = now - init;
        int mills = (int) timeInMills % 1000 / 10;
        int sec = (int) (timeInMills / 1000) % 60;
        int min = (int) (timeInMills / 1000) / 60 % 60;
        int hours = (int) (timeInMills / 1000) / 60 / 60;
        String time = String.format("%d:%02d:%02d:%02d", hours, min, sec, mills);
        Log.d(TAG, time + " " + hashCode());
        intent.putExtra(KEY_TIME, time);
        sendBroadcast(intent);
    }
}
