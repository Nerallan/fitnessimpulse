package com.senla.fitnessimpulse.util;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

public class GpsLocationService extends Service implements LocationListener {
    public static final String ACTION_LOCATION = GpsLocationService.class.getName();
    private static final String TAG = "GpsLocationService";
    private static final long MIN_TIME_BW_UPDATES = 5000; // The minimum time between updates in milliseconds
    private static final float MIN_DISTANCE_CHANGE_FOR_UPDATES = 5; // The minimum distance to change Updates in meters
    private static final String KEY_LOCATION = "com.senla.fitnessimpulse.fragment.LOCATION";
    protected LocationManager locationManager;

    private Intent locationIntent;

    final Handler handler = new Handler();
    private Context context;
    private Location location;
    double latitude;
    double longitude;

    boolean isGPSEnabled = false;

    public GpsLocationService() {
    }

    public GpsLocationService(Context context) {
        this.context = context;
        Log.d(TAG, "GpsLocationService: Constructor");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: ");
//        Toast.makeText(this, "Service location created", Toast.LENGTH_SHORT).show();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: ");
//        Toast.makeText(this, "Service location running", Toast.LENGTH_SHORT).show();
        locationIntent = new Intent(ACTION_LOCATION);
        startLocationTracking();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(runnable);
        runnable = null;
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
//        Toast.makeText(this, "Service location destroyed", Toast.LENGTH_SHORT).show();
        stopGpsLocation();
    }

    private void startLocationTracking() {
        handler.post(runnable);
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            location = getLocation();
            sendLocationBroadcast(location);
            handler.postDelayed(this, MIN_TIME_BW_UPDATES);
        }
    };

    public Location getLocation() {
        try {
            locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (isGPSEnabled) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    if(locationManager != null){
                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if(location != null){
                            getLatitude();
                            getLongitude();
                        }
                    }
                }
            }
        } catch (Exception exception){
            Log.d(TAG, "getLocation: " + exception.getMessage());
            exception.printStackTrace();
        }
        return location;
    }

    private void sendLocationBroadcast(Location location){
        if (location != null) {
            Log.d(TAG, "sendLocationBroadcast: " + location);
            locationIntent.putExtra(KEY_LOCATION, location);
        }
        sendBroadcast(locationIntent);
    }

    public void stopGpsLocation(){
        if(locationManager != null){
            locationManager.removeUpdates(GpsLocationService.this);
        }
    }

    public double getLatitude(){
        if(location != null){
            latitude = location.getLatitude();
        }
        return latitude;
    }

    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
        }
        return longitude;
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }
}
