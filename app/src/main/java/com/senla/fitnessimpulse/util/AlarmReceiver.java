package com.senla.fitnessimpulse.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.senla.fitnessimpulse.activity.TimerActivity;

public class AlarmReceiver extends BroadcastReceiver {
    private static final String TAG = "AlarmReceiver";
    private static final String NOTIFICATION_TITLE = "com.senla.fitnessimpulse.NOTIFICATION_TITLE";

    @Override
    public void onReceive(Context context, Intent intent) {
        //Trigger the notification
        NotificationScheduler notificationScheduler = new NotificationScheduler();
        if (intent.getExtras() != null){
            String title = intent.getExtras().getString(NOTIFICATION_TITLE);
            notificationScheduler.showNotification(context, TimerActivity.class,
                    title, "");
            Log.d(TAG, "onReceive: ");
        }
    }
}
