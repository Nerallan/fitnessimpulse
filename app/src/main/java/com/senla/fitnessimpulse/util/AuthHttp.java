package com.senla.fitnessimpulse.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.senla.fitnessimpulse.ErrorDeserializer;
import com.senla.fitnessimpulse.model.Error;
import com.senla.fitnessimpulse.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import bolts.TaskCompletionSource;
import okhttp3.CacheControl;
import okhttp3.Credentials;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AuthHttp {
    private static final String TAG = "AuthHttp";
    private static final String API_KEY = "AIzaSyArMh-kvKDEH_PQNGvUggP20gOCtAKsdvk";
    private static final String loginTokenUrl = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=" + API_KEY;
    private static final String registerTokenUrl = "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=" + API_KEY;

    private static final String CLOUD_JSON_USER_URL = "https://fitnessimpulse.firebaseio.com/user/";

    private HttpUserCallback httpCallback;
    private OkHttpClient okHttpClient;
    private String userId;

    // use Bolt promise library to chain network requests
    public AuthHttp(HttpUserCallback httpCallback){
        this.httpCallback = httpCallback;
        okHttpClient = new OkHttpClient();
    }

    public Task<Void> executeLoginTask(String email, String password){
        return Task.callInBackground(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return downloadAuthToken(loginTokenUrl, email, password);
            }
        }).onSuccess(new Continuation<String, com.senla.fitnessimpulse.model.Response>() {
            @Override
            public com.senla.fitnessimpulse.model.Response then(Task<String> task) throws Exception {
                com.senla.fitnessimpulse.model.Response responseObject = new Gson().fromJson(task.getResult(), com.senla.fitnessimpulse.model.Response.class);
                // error while getting auth token, process error
                if (responseObject.getIdToken() == null){
                    String errorMessage = parseError(task.getResult());
                    throw new Exception(errorMessage);
                }
                return responseObject;
            }
        }).onSuccessTask(new Continuation<com.senla.fitnessimpulse.model.Response, Task<String>>() {
            @Override
            public Task<String> then(Task<com.senla.fitnessimpulse.model.Response> task) throws Exception {
                if (task.getResult().getErrorMessage() != null){
                    throw new Exception(task.getResult().getErrorMessage());
                }
                userId = task.getResult().getLocalId();
                String stringUser = getCurrentUser(userId, task.getResult().getIdToken());
                TaskCompletionSource<String> successful = new TaskCompletionSource<>();
                successful.setResult(stringUser);
                return successful.getTask();
            }
        }).continueWith(new Continuation<String, Void>() {
            @Override
            public Void then(Task<String> task) throws Exception {
                if(task.isFaulted()){
                    httpCallback.onFailure(task.getError());
                } else {
                    User user = getUserFromStringResponse(task.getResult());
                    user.setUserId(userId);
                    httpCallback.onSuccess(user);
                }
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);
    }

    public Task<Void> executeRegistrationTask(String name, String surname, String email, String password){
        return Task.callInBackground(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return downloadAuthToken(registerTokenUrl, email, password);
            }
        }).onSuccess(new Continuation<String, com.senla.fitnessimpulse.model.Response>() {
            @Override
            public com.senla.fitnessimpulse.model.Response then(Task<String> task) throws Exception {
                com.senla.fitnessimpulse.model.Response responseObject = new Gson().fromJson(task.getResult(), com.senla.fitnessimpulse.model.Response.class);
                // error while getting auth token, process error
                if (responseObject.getIdToken() == null){
                    String errorMessage = parseError(task.getResult());
                    throw new Exception(errorMessage);
                }
                return responseObject;
            }
        }).onSuccessTask(new Continuation<com.senla.fitnessimpulse.model.Response, Task<String>>() {
            @Override
            public Task<String> then(Task<com.senla.fitnessimpulse.model.Response> task) throws Exception {
                if (task.getResult().getErrorMessage() != null){
                    throw new Exception(task.getResult().getErrorMessage());
                }
                userId = task.getResult().getLocalId();
                String registeredUser = addRegisteredUser(userId, task.getResult().getIdToken(), name, surname, email, password);
                TaskCompletionSource<String> successful = new TaskCompletionSource<>();
                successful.setResult(registeredUser);
                return successful.getTask();
            }
        }).continueWith(new Continuation<String, Void>() {
            @Override
            public Void then(Task<String> task) throws Exception {
                if(task.isFaulted()){
                    httpCallback.onFailure(task.getError());
                } else {
                    User user = getUserFromStringResponse(task.getResult());
                    user.setUserId(userId);
                    httpCallback.onSuccess(user);
                }
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);
    }

    private String parseError(String result) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Error.class, new ErrorDeserializer<Error>())
                .create();
        Error responseError = gson.fromJson(result, Error.class);
        return responseError.getMessage();
    }

    private String downloadAuthToken(String tokenUrl, String email, String password){
        RequestBody formBody = new FormBody.Builder()
                .add("email", email)
                .add("password", password)
                .add("returnSecureToken", "true")
                .build();

        Request request = new Request.Builder()
                // skip the cache, and fetch data directly from the server
                .cacheControl(new CacheControl.Builder().noCache().build())
                .url(tokenUrl)
                .header("Authorization", Credentials.basic(email, password))
                .header("Content-Type", "application/json")
                .post(formBody)
                .build();

        Response response;
        String stringResponse = null;
        try {
            response = okHttpClient.newCall(request).execute();
            stringResponse = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringResponse;
    }

    private String addRegisteredUser(String userId, String authToken, String name, String surname, String email, String password){
        String addUserUrl = CLOUD_JSON_USER_URL + userId + ".json";

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        JSONObject jsonObjectResp = new JSONObject();
        try {
            jsonObjectResp.put("name", name);
            jsonObjectResp.put("surname", surname);
            jsonObjectResp.put("email", email);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody requestBody = RequestBody.create(JSON, jsonObjectResp.toString());
        Request request = new Request.Builder()
                .url(addUserUrl)
                .header("Content-Type", "application/json")
                .put(requestBody)
                .build();

        Response response = null;
        String stringResponse = null;
        try {
            response = okHttpClient.newCall(request).execute();
            stringResponse = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringResponse;
    }

    public String getCurrentUser(String userId, String authToken){
        String userUrl = CLOUD_JSON_USER_URL + userId + ".json?auth=" + authToken;
        Request request = new Request.Builder()
                .url(userUrl)
                .build();

        Response response = null;
        String stringResponse = null;
        try {
            response = okHttpClient.newCall(request).execute();
            stringResponse = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringResponse;
    }

    private User getUserFromStringResponse(String userString){
        return new Gson().fromJson(userString, User.class);
    }
}
