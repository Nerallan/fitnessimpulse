package com.senla.fitnessimpulse.util;

import android.util.Log;

import com.senla.fitnessimpulse.model.Location;
import com.senla.fitnessimpulse.model.Track;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ServerTrackHttp {

    private static final String TAG = "ServerTrackHttp";
    private static final String CLOUD_JSON_TRACK_URL = "https://fitnessimpulse.firebaseio.com/track/";

    private OkHttpClient okHttpClient;
    private HttpTrackCallback httpTrackCallback;
    private HttpSyncCallback httpSyncCallback;

    public ServerTrackHttp(){
        this.okHttpClient = new OkHttpClient();
    }

    public void setHttpSyncCallback(HttpSyncCallback httpSyncCallback){
        this.httpSyncCallback = httpSyncCallback;
    }

    public void setHttpTrackCallback(HttpTrackCallback httpTrackCallback){
        this.httpTrackCallback = httpTrackCallback;
    }

    public Task<Void> executeAddTrackTask(String userId, Track track){
        return Task.callInBackground(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return getServerTrackCount(userId);
            }
        }).onSuccess(new Continuation<Integer, Void>() {
            // task.getResult - id in url for new track
            @Override
            public Void then(Task<Integer> task) throws Exception {
                return addTrackToServer(userId, track, task.getResult());
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(Task<Void> task) throws Exception {
                if (task.isFaulted()){
                    Log.d(TAG, "then: task failed" + task.getError());
                } else {
                    Log.d(TAG, "then: ");
                }
                return task.getResult();
            }
        });
    }

    public Task<List<Track>> executeGetTracksTask(String userId) {
        return Task.callInBackground(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return getTracks(userId);
            }
        }).onSuccess(new Continuation<String, List<Track>>() {
            @Override
            public List<Track> then(Task<String> task) throws Exception {
                return parseStringToTracks(task.getResult());
            }
        }).continueWith(new Continuation<List<Track>, List<Track>>() {
            @Override
            public List<Track> then(Task<List<Track>> task) throws Exception {
                if (task.isFaulted()){
                    Log.d(TAG, "then: task failed" + task.getError());
                    httpTrackCallback.onFailure(task.getError());
                } else {
                    Log.d(TAG, "then: ");
                    httpTrackCallback.onSuccess(task.getResult());
                }
                return task.getResult();
            }
        }, Task.UI_THREAD_EXECUTOR);
    }

    public Task<Void> executeDbWithServerSyncTask(String userId, List<Track> dbTrackList){
        return Task.callInBackground(new Callable<String>() {
            // get all tracks from server
            @Override
            public String call() throws Exception {
                return getTracks(userId);
            }
        }).onSuccess(new Continuation<String, List<Track>>() {
            @Override
            public List<Track> then(Task<String> task) throws Exception {
                if (task.getResult().equals("null")){
                    // if no track on server
                    return null;
                }
                return parseStringToTracks(task.getResult());
            }
        }).onSuccess(new Continuation<List<Track>, List<Track>>() {
            // get tracks that should be sync
            @Override
            public List<Track> then(Task<List<Track>> task) throws Exception {
                if (task.getResult() == null){
                    // no tracks on server, push all from db
                    return dbTrackList;
                } else {
                    List<Track> serverTrackList = task.getResult();
                    Collections.sort(serverTrackList, comparator);
                    long lastSyncDate = serverTrackList.get(0).getServerSyncTime();
                    return searchNotSyncTracks(lastSyncDate, dbTrackList);
                }
            }
        }).continueWith(new Continuation<List<Track>, List<Track>>() {
            @Override
            public List<Track> then(Task<List<Track>> task) throws Exception {
                if (task.isFaulted()){
                    Log.d(TAG, "then: task failed" + task.getError());
                } else {
                    Log.d(TAG, "then: ");
                }
                return task.getResult();
            }
        }).onSuccess(new Continuation<List<Track>, List<Track>>() {
            @Override
            public List<Track> then(Task<List<Track>> task) throws Exception {
               // TODO : if no tracks on server - push them all from db
                // Collect one task for each delete into an array.
                List<Task<Void>> tasks = new ArrayList<>();
                List<Track> tracksToSync = task.getResult();
                for (int index = 0; index < tracksToSync.size(); index++){
                    tasks.add(executeAddTrackTask(userId, tracksToSync.get(index)));
                }
                Task.whenAll(tasks);
                return tracksToSync;
            }
        }).continueWith(new Continuation<List<Track>, Void>() {
            @Override
            public Void then(Task<List<Track>> task) throws Exception {
                if (task.isFaulted()){
                    Log.d(TAG, "then: task failed" + task.getError());
                    httpSyncCallback.onSyncFailure(task.getError());
                } else {
                    Log.d(TAG, "then: ");
                    httpSyncCallback.onSyncSuccess(task.getResult());
                }
                return null;
            }
        }, Task.UI_THREAD_EXECUTOR);
    }

    private List<Track> searchNotSyncTracks(long lastSyncDate, List<Track> dbTrackList){
        List<Track> tracksForSync = new ArrayList<>();
        for(Track dbTrack : dbTrackList){
            if(dbTrack.getServerSyncTime() > lastSyncDate){
                // update sync time (Now there is stored the time of pressing the finish timer button)
                // supposition sync time
                dbTrack.setServerSyncTime(new Date().getTime());
                tracksForSync.add(dbTrack);
            }
        }
        return tracksForSync;
    }

    private Comparator<Track> comparator = new Comparator<Track>() {
        @Override
        public int compare(Track t1, Track t2) {
            if (t2.getServerSyncTime() > t1.getServerSyncTime()){
                return 1;
            } else if (t1.getServerSyncTime() > t2.getServerSyncTime()){
                return -1;
            } else {
                return 0;
            }
        }
    };

    private String getTracks(String userId){
        String trackCountUrl = CLOUD_JSON_TRACK_URL + userId + ".json";
        Request request = new Request.Builder()
                .url(trackCountUrl)
                .build();

        Response response = null;
        String stringResponse = null;
        try {
            response = okHttpClient.newCall(request).execute();
            stringResponse = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringResponse;
    }

    private Void addTrackToServer(String userId, Track track, int trackCount){
        String trackUrl = CLOUD_JSON_TRACK_URL + userId + "/" + trackCount + ".json";

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        JSONObject jsonObject = parseTrackToJson(track);
        RequestBody requestBody = RequestBody.create(JSON, jsonObject.toString());
        Request request = new Request.Builder()
                .url(trackUrl)
                .header("Content-Type", "application/json")
                .put(requestBody)
                .build();

        try {
            okHttpClient.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @link https://firebase.google.com/docs/database/rest/retrieve-data#shallow
     * @param userId
     */
    private int getServerTrackCount(String userId){
        String trackCountUrl = CLOUD_JSON_TRACK_URL + userId + ".json?shallow=true";
        Request request = new Request.Builder()
                .url(trackCountUrl)
                .build();

        Response response = null;
        String stringResponse = null;
        try {
            response = okHttpClient.newCall(request).execute();
            stringResponse = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return getCountFromString(stringResponse);
    }

    private int getCountFromString(String stringResponse) {
        int keys = 0;
        try {
            JSONObject mainObject = new JSONObject(stringResponse);
            keys = mainObject.length();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return keys;
    }

    private JSONObject parseTrackToJson(Track track){
        JSONObject jsonObjectTrack = new JSONObject();
        try {
            jsonObjectTrack.put("id_in_db", track.getId());
            jsonObjectTrack.put("distance", track.getRunDistance());
            JSONArray jsonArrayLocations = new JSONArray();
            for (int index = 0; index < track.getLocationList().size(); index++){
                JSONObject jsonObjectLocation = new JSONObject();
                jsonObjectLocation.put("latitude", String.valueOf(track.getLocationList().get(index).getLatitude()));
                jsonObjectLocation.put("longitude", String.valueOf(track.getLocationList().get(index).getLongitude()));
                jsonArrayLocations.put(jsonObjectLocation);
            }
            jsonObjectTrack.put("location_list", jsonArrayLocations);
            jsonObjectTrack.put("running_time", track.getRunTime());
            jsonObjectTrack.put("start_date", track.getStartDate());
            jsonObjectTrack.put("start_time", track.getStartTime());
            jsonObjectTrack.put("sync_timestamp", convertTimestampToString(track.getServerSyncTime()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObjectTrack;
    }

    private List<Track> parseStringToTracks(String tracksString){
        List<Track> trackList = new ArrayList<>();
        if (tracksString != null){
            try {
                JSONArray tracksArray = new JSONArray(tracksString);
                for (int index = 0; index < tracksArray.length(); index++){
                    JSONObject track = tracksArray.getJSONObject(index);
                    long idInDb = Long.parseLong(track.getString("id_in_db"));
                    String distance = track.getString("distance");
                    String startTime = track.getString("start_time");
                    String startDate = track.getString("start_date");
                    String runningTime = track.getString("running_time");
                    // if enable gps did not give location, the field location_list will not be written to server
                    List<Location> locationList = new ArrayList<>();
                    if(track.has("location_list")){
                        JSONArray locationsArray = track.getJSONArray("location_list");
                        if (locationsArray != null){
                            for (int prefix = 0; prefix < locationsArray.length(); prefix++){
                                JSONObject locationObject = locationsArray.getJSONObject(prefix);
                                double latitude = Double.parseDouble(locationObject.getString("latitude"));
                                double longitude = Double.parseDouble(locationObject.getString("longitude"));
                                locationList.add(new Location(latitude, longitude));
                            }
                        }
                    }
                    String syncTimeString = track.getString("sync_timestamp");
                    long serverLastSyncTime = convertStringToTimestamp(syncTimeString);
                    trackList.add(new Track(idInDb, startDate, startTime, distance, runningTime, locationList, serverLastSyncTime));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return trackList;
    }

    private long convertStringToTimestamp(String time){
        return Long.valueOf(time);
    }

    private String convertTimestampToString(long timestamp){
        return String.valueOf(timestamp);
    }
}
