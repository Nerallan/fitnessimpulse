package com.senla.fitnessimpulse.util;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.senla.fitnessimpulse.R;
import com.senla.fitnessimpulse.activity.MainActivity;

import java.util.Calendar;

import static android.app.Notification.DEFAULT_SOUND;
import static android.app.Notification.DEFAULT_VIBRATE;

public class NotificationScheduler {

    private static final int NOTIFICATION_SCHEDULER_REQUEST_CODE = 2;
    private static final String TAG = "NotificationScheduler";
    private static final String CHANNEL_ID = "com.senla.fitnessimpulse.NOTIFICATION_TITLE";
    private static final String NOTIFICATION_TITLE = "com.senla.fitnessimpulse.NOTIFICATION_TITLE";
    private static final String NOTIFICATION__CHANNEL_NAME = "fitness_notification";
    private static final int ACTIVITIES_REQUEST_CODE = 99;

    public NotificationScheduler(){
    }

    public void setNotification(Context context, Class<?> cls, String title, int year, int month, int day, int hour, int minute){
        Calendar calendar = setStartTime(year, month, day, hour, minute);

        // Enable a receiver
        PendingIntent pendingIntent = setReceiver(context, cls, title);
        setAlarmManager(context, pendingIntent, calendar);
    }

    private Calendar setStartTime(int year, int month, int day, int hour, int minute){
        Calendar calendar = Calendar.getInstance();
        Calendar setcalendar = Calendar.getInstance();
        setcalendar.set(Calendar.YEAR, year);
        setcalendar.set(Calendar.MONTH, month);
        setcalendar.set(Calendar.DAY_OF_MONTH, day);
        setcalendar.set(Calendar.HOUR_OF_DAY, hour);
        setcalendar.set(Calendar.MINUTE, minute);
        setcalendar.set(Calendar.SECOND, 0);
        // if notification didn't start for some reason -> start notification after 3 minutes
        if(setcalendar.before(calendar)){
            setcalendar.add(Calendar.MINUTE, 3);
        }
        return setcalendar;
    }

    private PendingIntent setReceiver(Context context, Class<?> cls, String title){
        Intent broadcastIntent = new Intent(context, cls);
        broadcastIntent.putExtra(NOTIFICATION_TITLE, title);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                NOTIFICATION_SCHEDULER_REQUEST_CODE,
                broadcastIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        return pendingIntent;
    }

    private void setAlarmManager(Context context, PendingIntent pendingIntent, Calendar calendar) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
        }
    }


    public void showNotification(Context context, Class<?> cls, String title, String content){
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent backIntent = new Intent(context, MainActivity.class);
        backIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        Intent notificationIntent = new Intent(context, cls);

//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
//        stackBuilder.addParentStack(cls);
          // Adds the Intent to the top of the stack
//        stackBuilder.addNextIntent(notificationIntent);
//        PendingIntent pendingIntent = stackBuilder.getPendingIntent(NOTIFICATION_SCHEDULER_REQUEST_CODE, PendingIntent.FLAG_UPDATE_CURRENT);

        PendingIntent pendingIntent = PendingIntent.getActivities(context, ACTIVITIES_REQUEST_CODE, new Intent[]{backIntent, notificationIntent}, PendingIntent.FLAG_ONE_SHOT);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null){
            NotificationChannel channel;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                // Important for heads-up notification
                channel = new NotificationChannel(CHANNEL_ID, NOTIFICATION__CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
                channel.enableLights(true);
                channel.setLightColor(Color.BLUE);
                channel.enableVibration(true);
                channel.setVibrationPattern(new long[]{0, 100, 200, 300});
                channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
                notificationManager.createNotificationChannel(channel);
            }
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID);
        //build notification
        Notification notification = builder.setContentTitle(title)
                .setContentText(content)
                .setAutoCancel(true)
                // Set ringtone and vibrations, important for heads-up notification
                .setDefaults(DEFAULT_SOUND | DEFAULT_VIBRATE)
                .setSound(alarmSound)
                .setLights(Color.BLUE, 500, 500)
                .setVibrate(new long[]{0, 100, 200, 300})
                .setSmallIcon(R.mipmap.ic_launcher_round)
                // to show heads-up notification
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent).build();

        if (notificationManager != null) {
            notificationManager.notify(NOTIFICATION_SCHEDULER_REQUEST_CODE, notification);
        }
    }
}
