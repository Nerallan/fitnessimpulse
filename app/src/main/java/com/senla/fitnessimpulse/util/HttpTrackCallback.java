package com.senla.fitnessimpulse.util;

import com.senla.fitnessimpulse.model.Track;

import java.util.List;

public interface HttpTrackCallback {

    /**
     * called when the server response was not 2xx or when an exception was thrown in the process
     * in case of server error (4xx, 5xx) this contains the server response
     * in case of IO exception this is null
     * @param throwable - contains the exception. in case of server error (4xx, 5xx) this is null
     */
    void onFailure(Throwable throwable);

    /**
     * contains the server response
     * @param trackList
     */
    void onSuccess(List<Track> trackList);
}
