package com.senla.fitnessimpulse.util;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

public class PermissionHandler {

	private static final int REQUEST_PERMISSIONS_CODE = 1;

	public PermissionHandler(){
	}

	public boolean isLocationPermissionGranted(Context context){
		if(context != null
				&& ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
				&& ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
			return false;
		}
		return true;
	}

	public void requestLocationPermission(@NonNull Fragment fragment){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			fragment.requestPermissions(new String[]{
					Manifest.permission.ACCESS_FINE_LOCATION,
					Manifest.permission.ACCESS_COARSE_LOCATION,
					Manifest.permission.INTERNET
			}, REQUEST_PERMISSIONS_CODE);
		}
	}

	public boolean isNetworkAvailable(Context context){
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
		return networkInfo != null && networkInfo.isConnected();
	}
}
