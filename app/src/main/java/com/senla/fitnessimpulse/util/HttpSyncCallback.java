package com.senla.fitnessimpulse.util;

import com.senla.fitnessimpulse.model.Track;

import java.util.List;

public interface HttpSyncCallback {
    /**
     * called when the server response was not 2xx or when an exception was thrown in the process
     * in case of server error (4xx, 5xx) this contains the server response
     * in case of IO exception this is null
     * @param throwable - contains the exception. in case of server error (4xx, 5xx) this is null
     */
    void onSyncFailure(Throwable throwable);

    /**
     * called when server db successfully sync with server
     * @param result track that was synced to server
     */
    void onSyncSuccess(List<Track> result);
}
