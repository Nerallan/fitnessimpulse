package com.senla.fitnessimpulse.repository;

import android.database.Cursor;

import com.senla.fitnessimpulse.database.DBManager;
import com.senla.fitnessimpulse.model.Notification;

public interface NotificationRepository {

    /**
     * @param position in recycler
     *
     * @return cursor returns particular notification
     */
    Cursor getNotificationById(long position);

    /**
     * @return cursor get list of all notifications
     */
    Cursor getAllNotifications();

    /**
     * @param notification specific item
     * @return boolean if data inserted incorrectly it will return -1
     */
    boolean addNotification(Notification notification);


    /**
     * @param  notification, notification specific item
     * @param  listener, listener for updates
     * @return boolean if data updated incorrectly it will return -1
     */
    boolean updateNotification(Notification notification, DBManager.SuccessfulUpdateListener listener);
}
