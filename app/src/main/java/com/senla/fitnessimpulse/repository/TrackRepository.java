package com.senla.fitnessimpulse.repository;

import android.database.Cursor;

import com.senla.fitnessimpulse.model.Track;

public interface TrackRepository {
    /**
     * Returns particular track
     * @return cursor
     */
    Cursor getTrackById(long position);
    /**
     * Returns list of tracks
     * @return cursor
     */
    Cursor getAllTracks();
    /**
     * if data inserted incorrectly it will return -1
     * @return boolean
     */
    boolean addTrack(Track track);
}
