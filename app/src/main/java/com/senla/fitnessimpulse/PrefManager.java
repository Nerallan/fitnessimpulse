package com.senla.fitnessimpulse;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.senla.fitnessimpulse.model.User;

public class PrefManager {

    private static final int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "com.senla.fitnessimpulse.pref_manager";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String USER_NAME = "name";
    private static final String USER_SURNAME = "surname";
    private static final String USER_EMAIL = "email";
    private static final String USER_ID = "userId";

    @Nullable
    private static PrefManager prefManager = null;
    @NonNull
    private SharedPreferences sharedPreferences;
    @NonNull
    private SharedPreferences.Editor editor;
    @NonNull
    private Context context;

    @NonNull
    public static PrefManager getInstance(Context context){
        if (prefManager == null) {
            prefManager = new PrefManager(context);
        }
        return prefManager;
    }

    private PrefManager(@NonNull Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.apply();
    }

    public boolean isFirstTimeLaunch() {
        return sharedPreferences.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void saveUser(User user){
        editor.putString(USER_ID, user.getUserId());
        editor.putString(USER_NAME, user.getName());
        editor.putString(USER_SURNAME, user.getSurname());
        editor.putString(USER_EMAIL, user.getEmail());
        editor.apply();
    }

    public User getUser(){
        String userId = sharedPreferences.getString(USER_ID, null);
        String name = sharedPreferences.getString(USER_NAME, null);
        String surname = sharedPreferences.getString(USER_SURNAME, null);
        String email = sharedPreferences.getString(USER_EMAIL, null);
        return new User(userId, name, surname, email);
    }

    public void logoutUser(){
        editor.remove(USER_ID);
        editor.remove(USER_NAME);
        editor.remove(USER_SURNAME);
        editor.remove(USER_EMAIL);
        editor.apply();
    }
}
