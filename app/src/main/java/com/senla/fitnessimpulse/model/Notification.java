package com.senla.fitnessimpulse.model;

public class Notification {
    private long id;
    private String title;
    private String date;
    private String time;

    public Notification(){
    }

    public Notification(String title, String date, String time) {
        this.title = title;
        this.date = date;
        this.time = time;
    }

    public Notification(long id, String title, String date, String time) {
        this.id = id;
        this.title = title;
        this.date = date;
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
