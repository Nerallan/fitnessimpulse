package com.senla.fitnessimpulse.model;

import java.util.List;

public class Track {
    // id is used to know what row should be updated in db after sync with server
    private long id;
    private String startDate;
    private String startTime;
    private String runDistance;
    private String runTime;
    private List<Location> locationList;
    private long serverSyncTime;

    public Track(){
    }

    public Track(String startDate, String startTime, String runDistance, String runTime) {
        this.startDate = startDate;
        this.startTime = startTime;
        this.runDistance = runDistance;
        this.runTime = runTime;
    }

    public Track(String startDate, String startTime, String runDistance, String runTime, List<Location> locationList, long serverSyncTime) {
        this.startDate = startDate;
        this.startTime = startTime;
        this.runDistance = runDistance;
        this.runTime = runTime;
        this.locationList = locationList;
        this.serverSyncTime = serverSyncTime;
    }

    public Track(long idInDb, String startDate, String startTime, String runDistance, String runTime, List<Location> locationList, long syncTimestamp) {
        this.id = idInDb;
        this.startDate = startDate;
        this.startTime = startTime;
        this.runDistance = runDistance;
        this.runTime = runTime;
        this.locationList = locationList;
        this.serverSyncTime = syncTimestamp;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getRunDistance() {
        return runDistance;
    }

    public void setRunDistance(String runDistance) {
        this.runDistance = runDistance;
    }

    public String getRunTime() {
        return runTime;
    }

    public void setRunTime(String runTime) {
        this.runTime = runTime;
    }

    public List<Location> getLocationList() {
        return locationList;
    }

    public void setLocationList(List<Location> locationList) {
        this.locationList = locationList;
    }

    public long getServerSyncTime() {
        return serverSyncTime;
    }

    public void setServerSyncTime(long serverSyncTime) {
        this.serverSyncTime = serverSyncTime;
    }
}
