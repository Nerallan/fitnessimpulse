package com.senla.fitnessimpulse.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.ViewGroup;

import com.senla.fitnessimpulse.R;
import com.senla.fitnessimpulse.adapter.holder.NotificationViewHolder;
import com.senla.fitnessimpulse.model.Notification;

public class NotificationRecyclerViewAdapter extends BaseRecyclerViewAdapter<Notification, OnRecyclerItemClickListener, NotificationViewHolder> {

    public NotificationRecyclerViewAdapter(Context context, OnRecyclerItemClickListener listener) {
        super(listener);
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new NotificationViewHolder(inflate(R.layout.notification_list_row, viewGroup), getListener());
    }

    public void remove(int position){
        super.remove(position);
    }
}
