package com.senla.fitnessimpulse.adapter.holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.senla.fitnessimpulse.adapter.BaseRecyclerListener;
import com.senla.fitnessimpulse.adapter.BaseRecyclerViewAdapter;


/**
 * Base ViewHolder to be used with the generic adapter.
 * {@link BaseRecyclerViewAdapter}
 *
 * @param <T> type of objects, which will be used in the adapter's data set
 * @param <L> click listener {@link BaseRecyclerListener}
 */
public abstract class BaseViewHolder<T, L extends BaseRecyclerListener> extends RecyclerView.ViewHolder {
    private L listener;

    public BaseViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public BaseViewHolder(View itemView, L listener){
        super(itemView);
        this.listener = listener;
    }

    /**
     * Bind data to the item.
     * should not perform any expensive operations here.
     * @param data object, associated with the item.
     */
    public abstract void bind(T data);

    protected L getListener() {
        return listener;
    }
}
