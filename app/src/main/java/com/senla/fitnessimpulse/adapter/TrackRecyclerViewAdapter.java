package com.senla.fitnessimpulse.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.ViewGroup;

import com.senla.fitnessimpulse.R;
import com.senla.fitnessimpulse.adapter.holder.TrackViewHolder;
import com.senla.fitnessimpulse.model.Track;

public class TrackRecyclerViewAdapter extends BaseRecyclerViewAdapter<Track, OnRecyclerItemClickListener, TrackViewHolder>{

    public TrackRecyclerViewAdapter(Context context, OnRecyclerItemClickListener listener){
        super(listener);
    }

    @NonNull
    @Override
    public TrackViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new TrackViewHolder(inflate(R.layout.track_list_row, viewGroup), getListener());
    }
}
