package com.senla.fitnessimpulse.adapter;

/**
 * Base recycler click listener contract.
 * To be implemented by all the click listeners in order to be used with the
 * {@link com.senla.fitnessimpulse.adapter.BaseRecyclerViewAdapter}
 */
public interface BaseRecyclerListener {
}
