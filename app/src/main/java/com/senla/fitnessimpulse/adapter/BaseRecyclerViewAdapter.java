package com.senla.fitnessimpulse.adapter;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.senla.fitnessimpulse.adapter.holder.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseRecyclerViewAdapter<T, L extends BaseRecyclerListener, VH extends BaseViewHolder<T, L>>  extends RecyclerView.Adapter<VH>{

    private List<T> dataList;
    private L listener;

    /**
     * Base constructor.
     */
    public BaseRecyclerViewAdapter(){
        dataList = new ArrayList<T>();
    }

    public BaseRecyclerViewAdapter(L listener){
        dataList = new ArrayList<T>();
        this.listener = listener;
    }

    /**
     * To be implemented in as specific adapter.
     * Here we return new ViewHolder instance.
     * may also return different ViewHolders according to a view type.
     * In this case should override {@link RecyclerView.Adapter#getItemViewType(int)}
     *
     * @param viewGroup The ViewGroup into which the new View will be added after it is bound to an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     */
    @Override
    public abstract VH onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType);

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the itemView to reflect the item at the given
     * position.
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(VH holder, int position) {
        if (dataList.size() <= position){
            return;
        }
        T item = dataList.get(position);
        holder.bind(item);
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return dataList != null ? dataList.size() : 0;
    }

    /**
     * Sets items to the adapter and notifies that data set has been changed.
     * @param dataList items to set to the adapter
     */
    public void setItems(List<T> dataList) {
        setItems(dataList, true);
        notifyDataSetChanged();
    }

    /**
     * Sets items to the adapter and notifies that data set has been changed.
     * Typically this method should be use with `notifyChanges = false` in case you are using DiffUtil
     * {@link android.support.v7.util.DiffUtil} in order to delegate it do all the updating job.
     *
     * @param dataList items to set to the adapter
     * @param notifyChanges pass in <code>true</code> to call notifiDatasetChanged {@link RecyclerView.Adapter#notifyDataSetChanged()} or <code>false</code> otherwise
     * @throws IllegalArgumentException in case of setting `null` items
     */
    public void setItems(List<T> dataList, boolean notifyChanges) throws IllegalArgumentException {
        if (dataList == null) {
            throw new IllegalArgumentException("Cannot set `null` item to the Recycler adapter");
        }
        this.dataList.clear();
        this.dataList.addAll(dataList);
        if (notifyChanges) {
            notifyDataSetChanged();
        }
    }

    /**
     * Updates items list.
     * Typically to be used for the implementation of DiffUtil {@link android.support.v7.util.DiffUtil}
     *
     * @param newDataList new items
     */
    public void updateItems(List<T> newDataList) {
        setItems(newDataList, false);
    }


    /**
     * Updates items with use of DiffUtil callback {@link DiffUtil.Callback}
     *
     * @param newItems     new items
     * @param diffCallback DiffUtil callback
     */
    public void updateItems(List<T> newItems, DiffUtil.Callback diffCallback) {
        DiffUtil.DiffResult result = DiffUtil.calculateDiff(diffCallback, false);
        setItems(newItems, false);
        result.dispatchUpdatesTo(this);
    }

    /**
     * Returns all items from the data set held by the adapter.
     *
     * @return All of items in this adapter.
     */
    public List<T> getDataList() {
        return dataList;
    }

    /**
     * Returns an items from the data set at a certain position.
     *
     * @return All of items in this adapter.
     */
    public T getData(int position) {
        return dataList.get(position);
    }

    /**
     * Clears all the items in the adapter.
     */
    public void clear() {
        dataList.clear();
        notifyDataSetChanged();
    }

    /**
     * Removes an item from the adapter.
     * Notifies that item has been removed.
     *
     * @param position of item to be removed
     */
    public void remove(int position) {
//        int position = dataList.indexOf(data);
        if (position > -1) {
            dataList.remove(position);
            notifyItemRemoved(position);
            notifyDataSetChanged();
        }
    }


    /**
     * Set click listener, which must extend {@link BaseRecyclerListener}
     *
     * @param listener click listener
     */
    public void setListener(L listener) {
        this.listener = listener;
    }

    /**
     * Get listener {@link BaseRecyclerListener}
     *
     * @return click listener
     */
    public L getListener() {
        return listener;
    }

    /**
     * Inflates a view.
     *
     * @param layout       layout to me inflater
     * @param parent       container where to inflate
     * @param attachToRoot whether to attach to root or not
     * @return inflated View
     */
    @NonNull
    protected View inflate(@LayoutRes final int layout, @Nullable final ViewGroup parent, final boolean attachToRoot) {
        return LayoutInflater.from(parent.getContext()).inflate(layout, parent, attachToRoot);
    }

    /**
     * Inflates a view.
     *
     * @param layout layout to me inflater
     * @param parent container where to inflate
     * @return inflated View
     */
    @NonNull
    protected View inflate(@LayoutRes final int layout, final @Nullable ViewGroup parent) {
        return inflate(layout, parent, false);
    }
}
