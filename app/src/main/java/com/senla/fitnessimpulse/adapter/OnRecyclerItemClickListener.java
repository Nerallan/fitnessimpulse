package com.senla.fitnessimpulse.adapter;

import android.support.v7.widget.RecyclerView;

/**
 * The most commonly used listener with the {@link BaseRecyclerListener}.
 * It is sutable for all cases where one click listener for a RecyclerView item is enough.
 */
public interface OnRecyclerItemClickListener extends BaseRecyclerListener {

    /**
     * Returns clicked item position {@link RecyclerView.ViewHolder#getAdapterPosition()}
     *
     * @param position clicked item position.
     */
    void onItemClick(int position);
}
