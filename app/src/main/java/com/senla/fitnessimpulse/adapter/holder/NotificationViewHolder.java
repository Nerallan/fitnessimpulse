package com.senla.fitnessimpulse.adapter.holder;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.senla.fitnessimpulse.R;
import com.senla.fitnessimpulse.adapter.OnRecyclerItemClickListener;
import com.senla.fitnessimpulse.model.Notification;

public class NotificationViewHolder extends BaseViewHolder<Notification, OnRecyclerItemClickListener> {

    @Nullable
    private TextView title;
    @Nullable
    private TextView date;
    @Nullable
    private TextView time;

    public NotificationViewHolder(@NonNull View itemView, OnRecyclerItemClickListener listener) {
        super(itemView, listener);
        initViews();
    }

    private void initViews() {
        title = itemView.findViewById(R.id.notification_title_text_view);
        date = itemView.findViewById(R.id.notification_date_text_view);
        time = itemView.findViewById(R.id.notification_time_text_view);
        if(getListener() != null){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getListener().onItemClick(getAdapterPosition());
                }
            });
        }
    }

    @Override
    public void bind(Notification data) {
        // bind data to the views
        if (title != null) {
            title.setText(data.getTitle());
        }
        if (date != null) {
            date.setText(data.getDate());
        }
        if (time != null) {
            time.setText(data.getTime());
        }
    }
}
