package com.senla.fitnessimpulse.adapter.holder;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.senla.fitnessimpulse.R;
import com.senla.fitnessimpulse.adapter.OnRecyclerItemClickListener;
import com.senla.fitnessimpulse.model.Track;

public class TrackViewHolder extends BaseViewHolder<Track, OnRecyclerItemClickListener> {

    @Nullable
    private TextView startTimeTextView;
    @Nullable
    private TextView startDateTextView;
    @Nullable
    private TextView distanceTextView;
    @Nullable
    private TextView runTimeTextView;

    public TrackViewHolder(@NonNull View itemView, OnRecyclerItemClickListener listener) {
        super(itemView, listener);
        initViews();
    }

    private void initViews() {
        this.startDateTextView = itemView.findViewById(R.id.start_date_text_view);
        this.startTimeTextView = itemView.findViewById(R.id.start_time_text_view);
        this.distanceTextView = itemView.findViewById(R.id.distance_text_view);
        this.runTimeTextView = itemView.findViewById(R.id.run_time_text_view);
        if(getListener() != null){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getListener().onItemClick(getAdapterPosition());
                }
            });
        }
    }

    @Override
    public void bind(Track track) {
        if (startDateTextView != null) {
            startDateTextView.setText(track.getStartDate());
        }
        if (startTimeTextView != null) {
            startTimeTextView.setText(track.getStartTime());
        }
        if (distanceTextView != null) {
            distanceTextView.setText(track.getRunDistance());
        }
        if (runTimeTextView != null) {
            runTimeTextView.setText(track.getRunTime());
        }
    }
}

